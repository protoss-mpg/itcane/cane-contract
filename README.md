[![Coverage](http://10.25.69.217:9000/api/project_badges/measure?project=com.mpg%3Adocumentservice&metric=coverage)](http://10.25.69.217:9000/dashboard?id=com.mpg%3Adocumentservice)

# documentservice
## docker sql server for ****dev 
```bash
$docker run --name sql-server-db -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=p@ssw0rd" -p 1401:1433 -d mcr.microsoft.com/mssql/server:2017-latest
```
```bash
$docker exec -it sql-server-db /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "p@ssw0rd" -Q "CREATE DATABASE CaneContract COLLATE Thai_CI_AS"
```

## Run Code coverage 
```bash
$mvn clean compile test sonar:sonar -Dsonar.host.url=http://10.25.69.217:9000
```
