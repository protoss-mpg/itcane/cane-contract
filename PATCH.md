#PATCH documentservice 

## Patch 0.003
```bash
- add module updatetruestatusappove in DocMainCOntroller
- add field flow_p_id in DocMain
- add field taskid and prossesid in DocMain
- add module create flow
- add field status_appove in DOcMain
- add module update signAppove
- add module select docMain muti
- fix hardCode year in Iservice in DocPromise
- fix bug docImg add try catch
- delete field sign and add in DocMain get data form Iservice in DocPromise
- add SignatureDirectorPic and HaedZone and Manager in DocMian
- add object in DocPromise
- add path bpimage in DocVerdor
- add module insertmuti in docMap
- fix bug new Data insert muti
- add module mutiUpdatepdf and mutiDeleteDoc in DocMap
- add module Signature
```

## Patch 0.002
```bash
- add field bp_spouse_sta and edit bp_spouse_pre in entity DocFarmer and DocNewFarmer 
```

## Patch 0.001
```bash
- fix module updateAttachHomeIdCardBookBank is service add mode Insert and Update
- add field bp_phone and bp_email in entity docFarmer and DocNewFarmer 
```