package com.mpg.documentservice;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.mpg.documentservice.entity.DocumentMap;
import com.mpg.documentservice.entity.DocumentPromise;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DocumentPromiseControllerTest {
    @Autowired
    private MockMvc mvc;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;




    @Test
    public void NoInsertIdTest() throws Exception {
        DocumentPromise documentPromise = new DocumentPromise();
        documentPromise.setId((long) 22);
        Gson g =new Gson();
        mvc.perform(MockMvcRequestBuilders.post("/api/documentservice/promise/v1")
                .content(g.toJson(documentPromise))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelError))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void warningInsertAttachNullTest() throws Exception {
        DocumentPromise documentPromise = new DocumentPromise();
        documentPromise.setBpCode("201047");
        Gson g =new Gson();
        mvc.perform(MockMvcRequestBuilders.post("/api/documentservice/promise/v1")
                .content(g.toJson(documentPromise))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelWarning))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void successInsertAttachNotNullTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/api/documentservice/promise/v1")
                .content(" {\n" +
                        "        \"attachIdCard\": {\n" +
                        "        \t\"imgPath\":\"1\"\n" +
                        "        },\n" +
                        "        \"attachHome\":{\n" +
                        "        \t\"imgPath\":\"2\"\n" +
                        "        },\n" +
                        "        \"attachBookBank\":{\n" +
                        "        \t\"imgPath\":\"3\"\n" +
                        "        },\n" +
                        "        \"attachLegalEntity\":{\n" +
                        "        \t\"imgPath\":\"4\"\n" +
                        "        },\n" +
                        "        \"attachCompanySeal\":{\n" +
                        "        \t\"imgPath\":\"5\"\n" +
                        "        }\n" +
                        "    }")
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelComplete))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void deleteTest() throws Exception {
        DocumentPromise documentpromise = new DocumentPromise();
        documentpromise.setDocYear("6263");
        documentpromise.setDocFac("3");
        documentpromise.setDocCreateId("00000");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/promise/v1")
                .content(g.toJson(documentpromise))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        String dataId = JsonPath.parse(response).read("$.dataId",String.class);

        mvc.perform(MockMvcRequestBuilders.delete("/api/documentservice/promise/v1/"+dataId)
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusMsg").value(labelSuccess))
                .andReturn();
    }

    @Test
    public void SuccessSelectByIdTest() throws Exception {
        DocumentMap documentMap = new DocumentMap();
        documentMap.setDocYear("6263");
        documentMap.setDocFac("3");
        documentMap.setDocCreateId("00016594");
        documentMap.setBpCode("201047");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/promise/v1")
                .content(g.toJson(documentMap))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        String id = JsonPath.parse(response).read("$.dataId", String.class);

        mvc.perform(MockMvcRequestBuilders.get("/api/documentservice/promisebyid/v1/" + id)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andReturn();
    }

    @Test
    public void SuccessSelectByDocIdTest() throws Exception {
        DocumentMap documentMap = new DocumentMap();
        documentMap.setDocYear("6263");
        documentMap.setDocFac("3");
        documentMap.setDocCreateId("00000");
        documentMap.setBpCode("201047");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/promise/v1")
                .content(g.toJson(documentMap))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        String docId = JsonPath.parse(response).read("$.docId");
        mvc.perform(MockMvcRequestBuilders.get("/api/documentservice/promisebydocid/v1/" + documentMap.getDocFac() + "/" + documentMap.getDocYear() + "/" + docId)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.docId").value(docId))
                .andReturn();
    }
}
