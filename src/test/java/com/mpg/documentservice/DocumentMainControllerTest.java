package com.mpg.documentservice;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.mpg.documentservice.entity.DocumentNewFarmer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DocumentMainControllerTest {
    @Autowired
    private MockMvc mvc;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;


    @Test
    public void selectMapMutiTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("2");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);


        mvc.perform(MockMvcRequestBuilders.get("/api/documentservice/docmainbydocid/v1/" + dataId + "-p10-t02")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(dataId))
                .andReturn();
    }

    @Test
    public void SuccessUpdateSignNotDiTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);


        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/approvenotdirector/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }

    @Test
    public void SuccessUpdateSignAllTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);


        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/approveall/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }

    @Test
    public void SuccessUpdateSignFarmerTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);
        documentNewFarmer1.setSignatureFarmerPic("R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==");
        documentNewFarmer1.setSignatureFarmerName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signfarmer/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }

    @Test
    public void ErrorUpdateSignFarmerTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId((long) 100000);
        documentNewFarmer1.setSignatureFarmerPic("ew");
        documentNewFarmer1.setSignatureFarmerName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signfarmer/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }

    @Test
    public void ErrorNoIdUpdateSignFarmerTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);


        documentNewFarmer1.setSignatureFarmerPic("ew");
        documentNewFarmer1.setSignatureFarmerName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signfarmer/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }

    @Test
    public void ErrorNoIdUpdateSignPersonalTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);


        documentNewFarmer1.setSignaturePersonalPic("ew");
        documentNewFarmer1.setSignaturePersonalName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signpersonal/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }

    @Test
    public void ErrorUpdateSignPersonalTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId((long) 100000);
        documentNewFarmer1.setSignatureFarmerPic("ew");
        documentNewFarmer1.setSignatureFarmerName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signpersonal/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }

    @Test
    public void SuccessUpdateSignPersonalTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);
        documentNewFarmer1.setSignatureFarmerPic("R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==");
        documentNewFarmer1.setSignatureFarmerName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signpersonal/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }


    @Test
    public void SuccessUpdatePathPdfTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("1");
        documentNewFarmer.setDocCreateId("00000");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);
        documentNewFarmer1.setPathPdf("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/pathpdf/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }

    @Test
    public void ErrorUpdatePathPdfTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("3");
        documentNewFarmer.setDocCreateId("00000");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId((long) 100000);

        documentNewFarmer1.setPathPdf("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/pathpdf/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }

    @Test
    public void ErrorNoIdUpdatePathPdfTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("3");
        documentNewFarmer.setDocCreateId("00000");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);


        documentNewFarmer1.setSignaturePersonalPic("ew");
        documentNewFarmer1.setSignaturePersonalName("abc");

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/pathpdf/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }


    @Test
    public void UpdateTrueStatusAppoveTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("2");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        documentNewFarmer.setStatusAppove("N");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/updatetruestatusappove/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }

    @Test
    public void ErrorTrueStatusAppoveTest() throws Exception {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        documentNewFarmer.setDocYear("6263");
        documentNewFarmer.setDocFac("2");
        documentNewFarmer.setDocCreateId("00016594");
        documentNewFarmer.setDocStatus("open");
        documentNewFarmer.setStatusAppove("a");
        Gson g = new Gson();

        MvcResult mvcResult = mvc.perform(post("/api/documentservice/farmer/v1")
                .content(g.toJson(documentNewFarmer))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelWarning))
                .andReturn();

        DocumentNewFarmer documentNewFarmer1 = new DocumentNewFarmer();

        String response = mvcResult.getResponse().getContentAsString();
        Long dataId = JsonPath.parse(response).read("$.dataId", Long.class);

        documentNewFarmer1.setId(dataId);

        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/updatetruestatusappove/v1")
                .content(g.toJson(documentNewFarmer1))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelError))
                .andReturn();
    }
}
