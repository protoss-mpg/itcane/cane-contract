package com.mpg.documentservice;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.mpg.documentservice.entity.SignatureEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SignatureControllerTest {
    @Autowired
    private MockMvc mvc;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;

    @Test
    public void InsertTest() throws Exception {
        SignatureEntity signatureEntity = new SignatureEntity();
        signatureEntity.setUserId("00016595");
        signatureEntity.setSignature("asdasddsadadasdasdasdasdasadsaddsaasddasdkkjkldsf");
        Gson g = new Gson();
        mvc.perform(MockMvcRequestBuilders.post("/api/documentservice/signature/v1")
                .content(g.toJson(signatureEntity))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelComplete))
                .andDo(MockMvcResultHandlers.print());


    }


    @Test
    public void DeleteTest() throws Exception {
        SignatureEntity signatureEntity = new SignatureEntity();
        signatureEntity.setUserId("00016596");
        signatureEntity.setSignature("asdasddsadadasdasdasdasdasadsaddsaasddasdkkjkldsf");
        Gson g = new Gson();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/documentservice/signature/v1")
                .content(g.toJson(signatureEntity))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        String dataId = JsonPath.parse(response).read("$.dataId", String.class);

        mvc.perform(MockMvcRequestBuilders.delete("/api/documentservice/signature/v1/" + dataId)
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
    }

    @Test
    public void UpdateTest() throws Exception {
        SignatureEntity signatureEntity = new SignatureEntity();
        signatureEntity.setUserId("00016594");
        signatureEntity.setSignature("asdasddsadadasdasdasdasdasadsaddsaasddasdkkjkldsf");
        Gson g = new Gson();
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/api/documentservice/signature/v1")
                .content(g.toJson(signatureEntity))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();
        String response = mvcResult.getResponse().getContentAsString();
        String id = JsonPath.parse(response).read("$.dataId", String.class);

        signatureEntity.setSignature("a");
        signatureEntity.setId(Long.parseLong(id));
        mvc.perform(MockMvcRequestBuilders.put("/api/documentservice/signature/v1")
                .content(g.toJson(signatureEntity))
                .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.codeTypeMsg").value(labelComplete))
                .andReturn();

        mvc.perform(MockMvcRequestBuilders.get("/api/documentservice/signaturebyid/v1/" + id)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.signature").value("a"))
                .andReturn();

        mvc.perform(MockMvcRequestBuilders.get("/api/documentservice/signaturebyuserid/v1/00016594" )
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").value("00016594"))
                .andReturn();
    }
}
