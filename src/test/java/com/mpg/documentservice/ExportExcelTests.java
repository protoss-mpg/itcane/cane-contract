package com.mpg.documentservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class ExportExcelTests {
	
	@Autowired
	 private WebApplicationContext wac;
	 private MockMvc mockMvc;
	 
	 @Before
	 public void setUp(){
	        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	 }

	 @After
	 public void tearDown() {
	 }

	@Test
	public void profileExport() throws Exception {
		String reportCode = "profileExport";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode)
				       .param("code", "3")
				)
                .andExpect(status().isOk())
                .andReturn().getResponse();
		
        Assert.assertNotEquals(0, response.getContentAsByteArray().length);
        Assert.assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", response.getContentType());
        Assert.assertEquals("attachment; filename=\"profile.xlsx\"", response.getHeader("content-disposition"));
        
        testGenOnLocal(response,"profile.xlsx");
	}
	

	@Test
	public void quotaYearDetailExport() throws Exception {
		String reportCode = "quotaYearDetailExport";
		MockHttpServletResponse response = mockMvc.perform(get("/export/"+reportCode)
				       .param("code", "3")
				)
                .andExpect(status().isOk())
                .andReturn().getResponse();
		
        Assert.assertNotEquals(0, response.getContentAsByteArray().length);
        Assert.assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", response.getContentType());
        Assert.assertEquals("attachment; filename=\"quotaYearDetails.xlsx\"", response.getHeader("content-disposition"));
        
        testGenOnLocal(response,"quotaYearDetails.xlsx");
	}
	
	private void testGenOnLocal(MockHttpServletResponse response,String fileName)  throws Exception{
		OutputStream os  = new FileOutputStream(new File("/Users/dev2/Desktop/shareExcel/"+fileName)); 

	    // Starts writing the bytes in it 
	    os.write(response.getContentAsByteArray()); 
	
	    // Close the file 
	    os.close();
	}

}




