package com.mpg.documentservice.export;

import com.mpg.documentservice.export.builder.ExportBuilder;
import com.mpg.documentservice.export.builder.ProfileExportBuilder;
import com.mpg.documentservice.export.builder.QuotaYearDetailExportBuilder;

public class ExportFactory {
	
	private ExportFactory(){
		super();
    }
	
    public static ExportBuilder builder(String reportCode){
    	ExportBuilder exportBuilder = null;
        switch (reportCode){
            case ExportConstant.REPORT_PROFILE_EXPORT :
            	exportBuilder = new ProfileExportBuilder(reportCode,ExportConstant.CONTENT_TYPE_EXCEL);
                break;
            default:
            	exportBuilder = new QuotaYearDetailExportBuilder(reportCode,ExportConstant.CONTENT_TYPE_EXCEL);
                break;
        }
        return  exportBuilder;
    }

}
