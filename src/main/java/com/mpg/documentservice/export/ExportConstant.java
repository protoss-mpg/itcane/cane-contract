package com.mpg.documentservice.export;

public class ExportConstant {
	
	private ExportConstant() {
		super();
	}
	public static final String EXPORT_TYPE_MULTI_EXCEL        = "mxlsx";
	public static final String EXPORT_TYPE_EXCEL              = "xlsx";
	public static final String EXPORT_TYPE_PDF                = "pdf";
	public static final String EXPORT_TYPE_CSV                = "csv";
	

	public static final String CONTENT_TYPE_EXCEL             = "vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String CONTENT_TYPE_PDF               = "pdf";
	public static final String CONTENT_TYPE_CSV               = "csv";
	
	public static final String REPORT_COLUMN_ALIGN_LEFT       = "LEFT";
	public static final String REPORT_COLUMN_ALIGN_CENTER     = "CENTER";
	public static final String REPORT_COLUMN_ALIGN_RIGHT      = "RIGHT";
	

	public static final String REPORT_PROFILE_EXPORT          = "profileExport";
	public static final String REPORT_QUOTA_YEAR_DETAIL_EXPORT= "quotaYearDetailExport";
}
