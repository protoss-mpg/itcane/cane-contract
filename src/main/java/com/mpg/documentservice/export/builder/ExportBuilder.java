package com.mpg.documentservice.export.builder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.mpg.documentservice.export.ExportConstant;
import com.mpg.documentservice.model.ExportProjectionModel;

import lombok.Data;

@Data
public abstract class ExportBuilder {
	
	
	private ByteArrayOutputStream outputStream;
	private String fileName;
	private String contentType;
	private String reportCode;
	private String reportName ;
	private String templateFileName;
	private String sqlStatement;
	private List   multiDataList;
	private Map<String,ExportProjectionModel> projectionMap;
	
	
	
	
	
	
	
	
	
	public ExportBuilder(String reportCode,String reportType) {
		 switch (reportType){
	         case ExportConstant.EXPORT_TYPE_MULTI_EXCEL :
	        	 this.reportCode = reportCode;
	        	 this.contentType = ExportConstant.CONTENT_TYPE_EXCEL;
	             break;
	         default:
	        	 this.contentType = ExportConstant.CONTENT_TYPE_EXCEL;
	        	 this.reportCode = reportCode;
	             break;
	     }
	}
	

	public ExportBuilder setDataListBuilder(List  multiDataList) {
		this.multiDataList = multiDataList;
		return this;
	}
	


	public ExportBuilder setReportName(String reportName) {
		this.reportName = reportName;
		return this;
	}
	


	public boolean isGenerateSuccess() throws IOException{
		boolean hasNotError = true;
		outputStream = writeData(multiDataList); 
		
		return hasNotError;
		
	}
	
	
	public ExportBuilder setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
		return this;
	}


	public abstract ByteArrayOutputStream writeData(List<Map<String,Object>> dataLs) throws IOException ;
	
	

}
