package com.mpg.documentservice.export.builder;

import java.util.HashMap;
import java.util.Map;

import com.mpg.documentservice.export.ExportConstant;
import com.mpg.documentservice.model.ExportProjectionModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuotaYearDetailExportBuilder extends ExportExcelTemplateBuilder{
	

	public QuotaYearDetailExportBuilder(String reportCode, String reportType) {
		super(reportCode, reportType);
		
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ");
		sb.append(" 	a.bp_code AS BP_CODE, ");				//0
		sb.append(" 	a.zone_id AS ZONE_ID, ");				//1
		sb.append(" 	a.sub_zone AS SUB_ZONE, ");				//2	
		sb.append(" 	a.doc_year AS DOC_YEAR, ");				//3
		sb.append(" 	b.doc_ton AS DOC_TON ");				//5	
		sb.append(" FROM tbl_document a JOIN tbl_document_promise b ON a.id=b.doc_id_ref ");
		sb.append(" WHERE a.doc_fac=:code  AND a.status_appove='Y'  ");
		this.setSqlStatement(sb.toString());
		log.debug("sql={}",sb.toString());
		
		/* Set Sequence of projection */
		Map<String, ExportProjectionModel> projectionMap = new HashMap();
		projectionMap.put("BP_CODE",		new ExportProjectionModel(0, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//0
		projectionMap.put("ZONE_ID",		new ExportProjectionModel(1, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//1
		projectionMap.put("SUB_ZONE",		new ExportProjectionModel(2, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//2
		projectionMap.put("DOC_YEAR",		new ExportProjectionModel(3, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//3
		projectionMap.put("DOC_TON",		new ExportProjectionModel(5, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//5
		this.setProjectionMap(projectionMap );
		
	}

	
}
