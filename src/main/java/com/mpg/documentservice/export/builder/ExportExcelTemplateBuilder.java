package com.mpg.documentservice.export.builder;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;

import com.mpg.documentservice.export.ExportConstant;
import com.mpg.documentservice.model.ExportProjectionModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExportExcelTemplateBuilder extends ExportBuilder{
	

	public ExportExcelTemplateBuilder(String reportCode, String reportType) {
		super(reportCode, reportType);
	}

	@Override
	public ByteArrayOutputStream writeData(List multiDataList)   throws IOException{
		List<Map<String, Object>> dataLs = (List<Map<String, Object>>) multiDataList.get(0);
		      /* Write to outPutStream */
		  ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		  
		  Workbook wb = null;
		  Sheet sheet = null;
		  if(this.getTemplateFileName()==null) {
			  wb = new XSSFWorkbook();
			  sheet = wb.createSheet("REPORT");
		  }else {
			  wb = new XSSFWorkbook(new FileInputStream(this.getTemplateFileName()));
			  sheet = wb.getSheet(this.getReportName());
		  }
	  	  
	  	  int startIndexRow = 0;
	  	  /* Set Header */
	  	  if(this.getTemplateFileName()==null) {
	  		  startIndexRow = 4;
	  		  Font fontHeader = wb.createFont();
		      fontHeader.setFontName("Arial");
		      fontHeader.setBold(true);
		      
		      CellStyle styleReportName = wb.createCellStyle();
		  	  styleReportName.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		  	  styleReportName.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		  	  styleReportName.setAlignment(HorizontalAlignment.CENTER);
		  	  styleReportName.setFont(fontHeader);
	  		  
	  		  
	  		  Row rowReportName = sheet.createRow(1);
		  	  Cell cellReportName = rowReportName.createCell(4);
		      cellReportName.setCellValue(new XSSFRichTextString(this.getReportName()));
		      cellReportName.setCellStyle(styleReportName);
	  	  }
	  	  
		      
		      Cell cell = null;
		      Map<String,Integer> mapKeyIndex = new HashMap(); 
		      Map<String,String>  mapKeyType  = new HashMap(); 
		      int countColumn = 0;
		      
		      /* Gen Header Column */
		      for(Entry<String,ExportProjectionModel> entry:this.getProjectionMap().entrySet()){
		    	  String key = entry.getKey();
		          mapKeyIndex.put(key, this.getProjectionMap().get(key).getIndexDisplay());
		          mapKeyType.put(key , this.getProjectionMap().get(key).getColumnAlign());
		          countColumn++;
		      }
		      
		      /*Set Data*/
		      int rowIndex = startIndexRow+1;
		      for(Map<String,Object> model:dataLs){
		      	Row rowData = sheet.createRow(rowIndex);
		      	
		      	for(Entry<String, Integer> entry:mapKeyIndex.entrySet()){
		      		String key = entry.getKey();
		      		cell = rowData.createCell(mapKeyIndex.get(key));
		      		  String content = null;
		      		  if(model.get(key) !=null) {
		      			content = String.valueOf(model.get(key));
		      		  }
		      		  cell.setCellValue(new XSSFRichTextString( content));
		              
		              CellStyle styleCell = wb.createCellStyle();
		              if(ExportConstant.REPORT_COLUMN_ALIGN_LEFT.equals(mapKeyType.get(key))){
		              	styleCell.setAlignment(HorizontalAlignment.LEFT);
		              	cell.setCellStyle(styleCell);
		              }else if(ExportConstant.REPORT_COLUMN_ALIGN_RIGHT.equals(mapKeyType.get(key))){
		              	styleCell.setAlignment(HorizontalAlignment.RIGHT);
		              	cell.setCellStyle(styleCell);
		              }else {
		              	styleCell.setAlignment(HorizontalAlignment.CENTER);
		              	cell.setCellStyle(styleCell);
		              }
		      	}
		      	rowIndex++;
		      }
		      
		      //autoSizeColumn
		      for(int colNum = 0; colNum<countColumn;colNum++)   
		    	  wb.getSheetAt(0).autoSizeColumn(colNum);
		      
		    
			try {
				wb.write(outputStream);
			}finally {
				wb.close();
			}
			
		return outputStream;
	}


}
