package com.mpg.documentservice.export.builder;

import java.util.HashMap;
import java.util.Map;

import com.mpg.documentservice.export.ExportConstant;
import com.mpg.documentservice.model.ExportProjectionModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProfileExportBuilder extends ExportExcelTemplateBuilder{
	

	public ProfileExportBuilder(String reportCode, String reportType) {
		super(reportCode, reportType);
		
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ");
		sb.append(" 	a.bp_code AS BP_CODE, ");					//0
		sb.append(" 	'QUOTA' AS BP_TYPE, ");						//1
		sb.append(" 	b.bp_full_name AS BP_FULL_NAME, ");			//5	
		sb.append(" 	'QUOTA' AS BP_FARMER_TYPE, ");				//19
		sb.append(" 	b.bp_pre AS BP_PRE_NAME, ");				//35	
		sb.append(" 	b.bp_name AS BP_NAME, ");					//36	
		sb.append(" 	b.bp_lname AS BP_LAST_NAME, ");				//37
		sb.append(" 	b.bp_birthday AS BP_BIRTH_DAY, ");			//40
		sb.append(" 	b.bp_phone AS BP_PHONE, ");					//43
		sb.append(" 	b.bp_email AS BP_EMAIL, ");					//44
		sb.append(" 	b.bp_spouse_sta AS BP_SPOUSE_STATUS, ");	//45
		sb.append(" 	b.bp_idcard AS BP_ID_CARD, ");				//46
		sb.append(" 	b.bp_idcard_end_date AS BP_ID_CARD_END_DATE, ");		//47
		sb.append(" 	b.bp_idcard_start_date AS BP_ID_CARD_START_DATE, ");	//48
		sb.append(" 	b.bp_house_number AS BP_HOUSE_NUMBER, ");	//50
		sb.append(" 	b.bp_house_no AS BP_HOUSE_MOO_NUMBER, ");	//53
		sb.append(" 	b.bp_sub_district AS BP_SUB_DISTRICT, ");	//56
		sb.append(" 	b.bp_district AS BP_DISTRICT, ");			//57
		sb.append(" 	b.bp_province AS BP_PROVINCE, ");			//58
		sb.append(" 	'ไทย' AS BP_COUNTRY, ");						//59
		sb.append(" 	b.bp_spouse_pre AS BP_SPOUSE_PRE, ");		//72
		sb.append(" 	b.bp_spouse_name AS BP_SPOUSE_NAME, ");		//73
		sb.append(" 	b.bp_spouse_lname AS BP_SPOUSE_LAST_NAME, ");			//74
		sb.append(" 	b.bp_spouse_idcard AS BP_SPOUSE_ID_CARD ");	//82
		sb.append(" FROM tbl_document a JOIN tbl_document_new_farmer b ON a.id=b.doc_id_ref ");
		sb.append(" WHERE a.doc_fac=:code  AND a.status_appove='Y'  ");
		this.setSqlStatement(sb.toString());
		log.debug("sql={}",sb.toString());
		
		/* Set Sequence of projection */
		Map<String, ExportProjectionModel> projectionMap = new HashMap();
		projectionMap.put("BP_CODE", 		new ExportProjectionModel(0, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//0
		projectionMap.put("BP_TYPE", 		new ExportProjectionModel(1, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//1
		projectionMap.put("BP_FULL_NAME", 	new ExportProjectionModel(5, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//5
		projectionMap.put("BP_FARMER_TYPE", new ExportProjectionModel(19, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//19
		projectionMap.put("BP_PRE_NAME", 	new ExportProjectionModel(35, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//35	
		projectionMap.put("BP_NAME", 		new ExportProjectionModel(36, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//36	
		projectionMap.put("BP_LAST_NAME", 	new ExportProjectionModel(37, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//37
		projectionMap.put("BP_BIRTH_DAY", 	new ExportProjectionModel(40, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//40
		projectionMap.put("BP_PHONE", 		new ExportProjectionModel(43, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//43
		projectionMap.put("BP_EMAIL", 		new ExportProjectionModel(44, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//44
		projectionMap.put("BP_SPOUSE_STATUS", 		new ExportProjectionModel(45, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//45
		projectionMap.put("BP_ID_CARD", 			new ExportProjectionModel(46, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//46
		projectionMap.put("BP_ID_CARD_END_DATE", 	new ExportProjectionModel(47, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//47
		projectionMap.put("BP_ID_CARD_START_DATE", 	new ExportProjectionModel(48, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//48
		projectionMap.put("BP_HOUSE_NUMBER", 		new ExportProjectionModel(50, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//50
		projectionMap.put("BP_HOUSE_MOO_NUMBER", 	new ExportProjectionModel(53, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//53
		projectionMap.put("BP_SUB_DISTRICT", 		new ExportProjectionModel(56, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//56
		projectionMap.put("BP_DISTRICT", 			new ExportProjectionModel(57, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//57
		projectionMap.put("BP_PROVINCE", 			new ExportProjectionModel(58, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//58
		projectionMap.put("BP_COUNTRY", 			new ExportProjectionModel(59, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//59
		projectionMap.put("BP_SPOUSE_PRE", 			new ExportProjectionModel(72, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//72
		projectionMap.put("BP_SPOUSE_NAME", 		new ExportProjectionModel(73, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//73
		projectionMap.put("BP_SPOUSE_LAST_NAME", 	new ExportProjectionModel(74, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//74
		projectionMap.put("BP_SPOUSE_ID_CARD", 		new ExportProjectionModel(82, ExportConstant.REPORT_COLUMN_ALIGN_CENTER));	//82
		
		this.setProjectionMap(projectionMap );
		
	}

	
}
