package com.mpg.documentservice.controller;


import com.mpg.documentservice.entity.DocumentPromise;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.service.DocumentPromiseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class DocumentPromiseController {
    @Autowired
    private DocumentPromiseService documentPromiseService;

    @PostMapping(value = "/promise/v1", consumes = "application/json")
    public DocumentResponse insertFarmer(@RequestBody DocumentPromise data) {
        log.info("initPath=/api/documentservice/promise/v1.post");
        return documentPromiseService.insertPromise(data);
    }
    @DeleteMapping(value = "/promise/v1/{data}")
    public DocumentResponse deleteNewFarmer(@PathVariable String data) {
        log.info("initPath=/api/documentservice/promise/v1.delete");
        return documentPromiseService.deletePromise(data);
    }

    @GetMapping(value = "/promisebydocid/v1/{fac}/{year}/{docId}")
    public DocumentPromise selectNewFarmerByDocId(@PathVariable String docId, @PathVariable String fac, @PathVariable String year) {
        log.info("initPath=/api/documentservice/promisebydocid/v1/{}/{}/{}.get",fac,year,docId);
        return documentPromiseService.selectPromiseByDocId(docId,fac,year);
    }
    @GetMapping(value = "/promisebyid/v1/{id}")
    public DocumentPromise selectNewFarmerById(@PathVariable Long id) {
        log.info("initPath=/api/documentservice/promisebyid/v1/{}.get",id);
        return documentPromiseService.selectPromiseById(id);
    }
}
