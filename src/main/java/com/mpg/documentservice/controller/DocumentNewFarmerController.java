package com.mpg.documentservice.controller;


import com.mpg.documentservice.entity.DocumentNewFarmer;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.service.DocumentNewFarmerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class DocumentNewFarmerController {

    @Autowired
    private DocumentNewFarmerService documentNewFarmerService;

    @PostMapping(value = "/newfarmer/v1", consumes = "application/json")
    public DocumentResponse insertNewFarmer(@RequestBody DocumentNewFarmer data) {
        log.info("initPath=/api/documentservice/newfarmer/v1.post");
        return documentNewFarmerService.insertNewFarmer(data);
    }
    @PutMapping(value = "/newfarmerattach/v1", consumes = "application/json")
    public DocumentResponse updateAttach(@RequestBody DocumentNewFarmer data) {
        log.info("initPath=/api/documentservice/newfarmerattach/v1.put");
        return documentNewFarmerService.updateAttachHomeIdCardBookBank(data);
    }
    @DeleteMapping(value = "/newfarmer/v1/{data}")
    public DocumentResponse deleteNewFarmer(@PathVariable String data) {
        log.info("initPath=/api/documentservice/newfarmer/v1.delete");
        return documentNewFarmerService.deleteNewFarmer(data);
    }

    @GetMapping(value = "/newfarmerbydocid/v1/{fac}/{year}/{docId}")
    public DocumentNewFarmer selectNewFarmerByDocId(@PathVariable String docId, @PathVariable String fac, @PathVariable String year) {
        log.info("initPath=/api/documentservice/newfarmerbydocid/v1/{}/{}/{}.get",fac,year,docId);
        return documentNewFarmerService.selectNewFarmerByDocId(docId,fac,year);
    }
    @GetMapping(value = "/newfarmerbyid/v1/{id}")
    public DocumentNewFarmer selectNewFarmerById(@PathVariable Long id) {
        log.info("initPath=/api/documentservice/newfarmerbyid/v1/{}.get",id);
        return documentNewFarmerService.selectNewFarmerById(id);
    }
}
