package com.mpg.documentservice.controller;

import com.mpg.documentservice.entity.DocumentFarmer;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.service.DocumentMainService;
import com.mpg.documentservice.util.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class DocumentMainController {
    @Autowired
    private DocumentMainService documentMainService;

    @PutMapping(value = "/signfarmer/v1", consumes = "application/json")
    public DocumentResponse signFarmer(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/signfarmer/v1.put");
        return documentMainService.updateSignFarmer(data);
    }

    @PutMapping(value = "/signpersonal/v1", consumes = "application/json")
    public DocumentResponse signPersonal(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/signpersonal/v1.put");
        return documentMainService.updateSignPersonal(data);
    }

    @PutMapping(value = "/pathpdf/v1", consumes = "application/json")
    public DocumentResponse signPathPdf(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/pathpdf/v1.put");
        return documentMainService.updatePDF(data);
    }

    @PutMapping(value = "/approvenotdirector/v1", consumes = "application/json")
    public DocumentResponse ApproveNotDirector(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/approvenotdirector/v1.put");
        return documentMainService.updateSignApproveNotDirector(data);
    }

    @PutMapping(value = "/approveall/v1", consumes = "application/json")
    public DocumentResponse ApproveAll(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/approveall/v1.put");
        return documentMainService.updateSignApproveAll(data);
    }

    @GetMapping(value = "/docmainbydocid/v1/{id}")
    public ResponseEntity<String> selectMapById(@PathVariable String id) {
        log.info("initPath=/api/documentservice/docmainbydocid/v1/{}.get", id);
        String requestJsonBody = JSONUtil.getJSONSerializer().serialize(documentMainService.selectByDocId(id));
        return ResponseEntity.ok(requestJsonBody);
    }

    @PutMapping(value = "/updatetruestatusappove/v1", consumes = "application/json")
    public DocumentResponse updateTrueStatusAppove(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/updatetruestatusappove/v1.put");
        return documentMainService.updateTrueStatusAppove(data);
    }
}
