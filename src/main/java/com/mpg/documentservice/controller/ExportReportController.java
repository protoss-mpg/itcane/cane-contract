package com.mpg.documentservice.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.mpg.documentservice.service.ExportReportService;
import com.protosstechnology.commons.util.RequestUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ExportReportController {
	
	@Autowired
	ExportReportService exportReportService;
	
	@GetMapping("/export/{reportCode}")
    public ResponseEntity exportReport(HttpServletRequest request,@PathVariable String reportCode) {
		Map<String,String> mapParam = RequestUtil.getMapParam(request.getParameterMap());
		return exportReportService.genReportByReportCode(reportCode,mapParam);
		
	}
	
	

}
