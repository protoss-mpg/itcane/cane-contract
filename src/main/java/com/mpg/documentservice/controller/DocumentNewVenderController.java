package com.mpg.documentservice.controller;

import com.mpg.documentservice.entity.DocumentNewVender;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.service.DocumentNewVenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class DocumentNewVenderController {

    @Autowired
    private DocumentNewVenderService documentNewVenderService;

    @PostMapping(value = "/newvender/v1", consumes = "application/json")
    public DocumentResponse insertNewVender(@RequestBody DocumentNewVender data) {
        log.info("initPath=/api/documentservice/newvender/v1.post");
        return documentNewVenderService.insertNewVender(data);
    }

    @PutMapping(value = "/newvenderattach/v1", consumes = "application/json")
    public DocumentResponse updateAttach(@RequestBody DocumentNewVender data) {
        log.info("initPath=/api/documentservice/newfarmerattach/v1.put");
        return documentNewVenderService.updateAttachFile(data);
    }
    @DeleteMapping(value = "/newvender/v1/{data}")
    public DocumentResponse deleteNewVender(@PathVariable String data) {
        log.info("initPath=/api/documentservice/newfarmer/v1.delete");
        return documentNewVenderService.deleteNewVender(data);
    }

    @GetMapping(value = "/newvenderydocid/v1/{fac}/{year}/{docId}")
    public DocumentNewVender selectNewVenderByDocId(@PathVariable String docId, @PathVariable String fac, @PathVariable String year) {
        log.info("initPath=/api/documentservice/newvenderydocid/v1/{}/{}/{}.get",fac,year,docId);
        return documentNewVenderService.selectNewVenderByDocId(docId,fac,year);
    }
    @GetMapping(value = "/newvenderbyid/v1/{id}")
    public DocumentNewVender selectNewVenderById(@PathVariable Long id) {
        log.info("initPath=/api/documentservice/newvenderbyid/v1/{}.get",id);
        return documentNewVenderService.selectNewVenderById(id);
    }
}
