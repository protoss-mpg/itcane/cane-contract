package com.mpg.documentservice.controller;


import com.mpg.documentservice.entity.DocumentMap;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.model.MapMutiResponse;
import com.mpg.documentservice.service.DocumentMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class DocumentMapController {
    @Autowired
    private DocumentMapService documentMapService;

    @PostMapping(value = "/map/v1", consumes = "application/json")
    public DocumentResponse insertMap(@RequestBody DocumentMap data) {
        log.info("initPath=/api/documentservice/map/v1.post");
        return documentMapService.insertMap(data);
    }
    @DeleteMapping(value = "/map/v1/{data}")
    public DocumentResponse deleteMap(@PathVariable String data) {
        log.info("initPath=/api/documentservice/map/v1.delete");
        return documentMapService.deleteMap(data);
    }
    @GetMapping(value = "/mapbydocid/v1/{fac}/{year}/{docId}")
    public DocumentMap selectMapByDocId(@PathVariable String docId, @PathVariable String fac, @PathVariable String year) {
        log.info("initPath=/api/documentservice/mapbydocid/v1/{}/{}/{}.get",fac,year,docId);
        return documentMapService.selectMapByDocId(docId,fac,year);
    }
    @GetMapping(value = "/mapbyid/v1/{id}")
    public DocumentMap selectMapById(@PathVariable Long id) {
        log.info("initPath=/api/documentservice/mapbyid/v1/{}.get",id);
        return documentMapService.selectMapById(id);
    }
    @DeleteMapping(value = "/mutimap/v1")
    public MapMutiResponse deleteMutiMap(@RequestBody DocumentMap data) {
        log.info("initPath=/api/documentservice/mutimap/v1.delete");
        return documentMapService.deleteMutiMap(data);
    }

    @PostMapping(value = "/mutimap/v1", consumes = "application/json")
    public MapMutiResponse insertMapMunti(@RequestBody DocumentMap data) {
        log.info("initPath=/api/documentservice/mutimap/v1.post");
        return documentMapService.insertMapMuti(data);
    }

    @PutMapping(value = "/mutimappdf/v1", consumes = "application/json")
    public MapMutiResponse updateMapMunti(@RequestBody DocumentMap data) {
        log.info("initPath=/api/documentservice/mutimappdf/v1.put");
        return documentMapService.updateMutiPDF(data);
    }
}
