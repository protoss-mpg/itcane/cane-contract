package com.mpg.documentservice.controller;

import com.mpg.documentservice.entity.SignatureEntity;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.service.SignatureService;
import com.mpg.documentservice.util.EmptyJsonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class SignatureController {
    @Autowired
    private SignatureService signatureService;

    @PostMapping(value = "/signature/v1", consumes = "application/json")
    public DocumentResponse insertSignature(@RequestBody SignatureEntity data) {
        log.info("initPath=/api/documentservice/signature/v1.post");
        return signatureService.insertSignature(data);
    }

    @PutMapping(value = "/signature/v1", consumes = "application/json")
    public DocumentResponse updateSignature(@RequestBody SignatureEntity data) {
        log.info("initPath=/api/documentservice/signature/v1.put");
        return signatureService.updateSignature(data);
    }

    @DeleteMapping(value = "/signature/v1/{id}")
    public DocumentResponse deleteSignature(@PathVariable String id) {
        log.info("initPath=/api/documentservice/signature/v1.delete");
        return signatureService.deleteSignature(id);
    }

    @GetMapping(value = "/signaturebyuserid/v1/{userId}")
    public ResponseEntity selectSignatureByUserId(@PathVariable String userId) {
        log.info("initPath=/api/documentservice/signaturebyuserid/v1/{}.get", userId);
        return  ResponseEntity.ok(signatureService.selectSignatureByUserId(userId).isPresent() ? signatureService.selectSignatureByUserId(userId) :new EmptyJsonResponse());

    }

    @GetMapping(value = "/signaturebyid/v1/{id}")
    public ResponseEntity selectSignatureById(@PathVariable Long id) {
        log.info("initPath=/api/documentservice/signaturebyid/v1/{}.get", id);
        return  ResponseEntity.ok(signatureService.selectSignatureById(id).isPresent() ? signatureService.selectSignatureById(id) :new EmptyJsonResponse());
    }
}
