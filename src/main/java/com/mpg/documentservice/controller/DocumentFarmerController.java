package com.mpg.documentservice.controller;

import com.mpg.documentservice.entity.DocumentFarmer;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.service.DocumentFarmerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/documentservice")
public class DocumentFarmerController {
    @Autowired
    private DocumentFarmerService documentFarmerService;

    @PostMapping(value = "/farmer/v1", consumes = "application/json")
    public DocumentResponse insertFarmer(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/farmer/v1.post");
        return documentFarmerService.insertFarmer(data);
    }
    @PutMapping(value = "/farmerattach/v1", consumes = "application/json")
    public DocumentResponse updateAttach(@RequestBody DocumentFarmer data) {
        log.info("initPath=/api/documentservice/farmerattach/v1.put");
        return documentFarmerService.updateAttachHomeIdCardBookBank(data);
    }
    @DeleteMapping(value = "/farmer/v1/{data}")
    public DocumentResponse deleteFarmer(@PathVariable String data) {
        log.info("initPath=/api/documentservice/farmer/v1.delete");
        return documentFarmerService.deleteFarmer(data);
    }

    @GetMapping(value = "/farmerbydocid/v1/{fac}/{year}/{docId}")
    public DocumentFarmer selectFarmerByDocId(@PathVariable String docId, @PathVariable String fac, @PathVariable String year) {
        log.info("initPath=/api/documentservice/farmerbydocid/v1/{}/{}/{}.get",fac,year,docId);
        return documentFarmerService.selectFarmerByDocId(docId,fac,year);
    }
    @GetMapping(value = "/farmerbyid/v1/{id}")
    public DocumentFarmer selectFarmerById(@PathVariable Long id) {
        log.info("initPath=/api/documentservice/farmerbyid/v1/{}.get",id);
        return documentFarmerService.selectFarmerById(id);
    }

}
