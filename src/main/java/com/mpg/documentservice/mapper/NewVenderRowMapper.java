package com.mpg.documentservice.mapper;

import com.mpg.documentservice.entity.*;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewVenderRowMapper implements RowMapper {

    @Override
    public DocumentNewVender mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocumentNewVender documentNewVender = new DocumentNewVender();
        AttachBookBank attachBookBank = new AttachBookBank();
        AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
        AttachHome attachHome = new AttachHome();
        AttachIdCard attachIdCard = new AttachIdCard();
        AttachLegalEntity attachLegalEntity = new AttachLegalEntity();

        documentNewVender.setId(rs.getLong("id"));
        documentNewVender.setDocId(rs.getString("doc_id"));
        documentNewVender.setDocCreateDate(rs.getTimestamp("doc_create_date"));
        documentNewVender.setDocCreateId(rs.getString("doc_create_id"));
        documentNewVender.setDocCreateName(rs.getString("doc_create_name"));
        documentNewVender.setDocCreatePosition(rs.getString("doc_create_position"));
        documentNewVender.setDocDetail(rs.getString("doc_detail"));
        documentNewVender.setDocFac(rs.getString("doc_fac"));
        documentNewVender.setDocId(rs.getString("doc_id"));
        documentNewVender.setDocImage(rs.getString("doc_image"));
        documentNewVender.setDocStatus(rs.getString("doc_status"));
        documentNewVender.setDocTypeId(rs.getString("doc_type_id"));
        documentNewVender.setDocYear(rs.getString("doc_year"));
        documentNewVender.setSignaturePersonalDate(rs.getTimestamp("signature_personal_date"));
        documentNewVender.setSignaturePersonalName(rs.getString("signature_personal_name"));
        documentNewVender.setSignaturePersonalPic(rs.getString("signature_personal_pic"));
        documentNewVender.setSignaturePersonalPos(rs.getString("signature_personal_pos"));
        documentNewVender.setSignatureFarmerName(rs.getString("signature_farmer_name"));
        documentNewVender.setSignatureFarmerPic(rs.getString("signature_farmer_pic"));
        documentNewVender.setSignatureFarmerPos(rs.getString("signature_farmer_pos"));
        documentNewVender.setSignatureFarmerDate(rs.getTimestamp("signature_farmer_date"));

        documentNewVender.setDocIdRef(rs.getString("doc_id"));
        documentNewVender.setBpCodeDoc(rs.getString("bp_code"));
        documentNewVender.setBpCode(rs.getString("bp_code"));
        documentNewVender.setBpIdCard(rs.getString("bp_idcard"));
        documentNewVender.setBpPre(rs.getString("bp_pre"));
        documentNewVender.setBpName(rs.getString("bp_name"));
        documentNewVender.setBpLname(rs.getString("bp_lname"));
        documentNewVender.setBpFullName(rs.getString("bp_full_name"));
        documentNewVender.setBpImage(rs.getString("bp_image"));
        documentNewVender.setBpHouseNumber(rs.getString("bp_house_number"));
        documentNewVender.setBpHouseNo(rs.getString("bp_house_no"));
        documentNewVender.setBpSubDistrict(rs.getString("bp_sub_district"));
        documentNewVender.setBpDistrict(rs.getString("bp_district"));
        documentNewVender.setBpProvince(rs.getString("bp_province"));
        documentNewVender.setBpBirthday(rs.getString("bp_birthday"));
        documentNewVender.setBpIdCardStartDate(rs.getString("bp_idcard_start_date"));
        documentNewVender.setBpIdCardEndDate(rs.getString("bp_idcard_end_date"));
        documentNewVender.setBpCompanyIdCard(rs.getString("bp_company_idcard"));
        documentNewVender.setBpCompanyName(rs.getString("bp_company_name"));
        documentNewVender.setBpCompanyDate(rs.getString("bp_company_date"));
        documentNewVender.setBpCompanySta(rs.getString("bp_company_sta"));
        documentNewVender.setBpPhone(rs.getString("bp_phone"));
        documentNewVender.setBpEmail(rs.getString("bp_email"));
        documentNewVender.setBpDateCreate(rs.getTimestamp("bp_date_create"));
        documentNewVender.setBpDateUpdate(rs.getTimestamp("bp_date_update"));
        documentNewVender.setSignatureHeadzoneId(rs.getString("signature_head_zone_id"));
        documentNewVender.setSignatureHeadzoneDate(rs.getTimestamp("signature_head_zone_date"));
        documentNewVender.setSignatureDirectorId(rs.getString("signature_director_id"));
        documentNewVender.setSignatureDirectorDate(rs.getTimestamp("signature_director_date"));
        documentNewVender.setStatusAppove(rs.getString("status_appove"));
        documentNewVender.setSignatureDirectorFullName(rs.getString("name_director"));
        documentNewVender.setSignatureDirectorPic(rs.getString("id_director"));

        documentNewVender.setSignatureHeadzoneFullName(rs.getString("name_head_zone"));
        documentNewVender.setSignatureHeadzonePic(rs.getString("id_head_zone"));

        documentNewVender.setSignatureManagerFullName(rs.getString("name_manager"));
        documentNewVender.setSignatureManagerPic(rs.getString("id_manager"));

        documentNewVender.setZoneId(rs.getString("zone_id"));
        documentNewVender.setSubZone(rs.getString("sub_zone"));
        documentNewVender.setPathPdf(rs.getString("path_pdf"));
        documentNewVender.setStatusPdf(rs.getString("status_pdf"));

        documentNewVender.setFacName(rs.getString("fac_name"));
        documentNewVender.setFacHome(rs.getString("fac_home"));
        documentNewVender.setFacVillageNo(rs.getString("fac_village_no"));
        documentNewVender.setFacSubDistrict(rs.getString("fac_sub_district"));
        documentNewVender.setFacDistrict(rs.getString("fac_district"));
        documentNewVender.setFacProvince(rs.getString("fac_province"));
        documentNewVender.setBranch(rs.getString("fac_branch"));
        documentNewVender.setFacHeadPre(rs.getString("fac_head_pre"));
        documentNewVender.setFacHeadName(rs.getString("fac_head_name"));
        documentNewVender.setFacHeadLname(rs.getString("fac_head_lname"));
        documentNewVender.setFacCode(rs.getString("fac_code"));
        documentNewVender.setFacQueCode(rs.getString("fac_que_code"));

        attachBookBank.setId(rs.getLong("id_att_book_bank"));
        attachBookBank.setCreateTime(rs.getTimestamp("create_time_att_book_bank"));
        attachBookBank.setDocId(rs.getString("doc_id_att_book_bank"));
        attachBookBank.setImgPathFront(rs.getString("img_path_front_att_book_bank"));
        attachBookBank.setImgPathBack(rs.getString("img_path_back_att_book_bank"));

        attachCompanySeal.setId(rs.getLong("id_att_company_seal"));
        attachCompanySeal.setCreateTime(rs.getTimestamp("create_time_att_company_seal"));
        attachCompanySeal.setDocId(rs.getString("doc_id_att_company_seal"));
        attachCompanySeal.setImgPath(rs.getString("img_path_att_company_seal"));

        attachHome.setId(rs.getLong("id_att_home"));
        attachHome.setCreateTime(rs.getTimestamp("create_time_att_home"));
        attachHome.setDocId(rs.getString("doc_id_att_home"));
        attachHome.setImgPathFront(rs.getString("img_path_front_att_home"));
        attachHome.setImgPathBack(rs.getString("img_path_back_att_home"));

        attachIdCard.setId(rs.getLong("id_att_idcard"));
        attachIdCard.setCreateTime(rs.getTimestamp("create_time_att_idcard"));
        attachIdCard.setDocId(rs.getString("doc_id_att_idcard"));
        attachIdCard.setImgPathFront(rs.getString("img_path_front_att_idcard"));
        attachIdCard.setImgPathBack(rs.getString("img_path_back_att_idcard"));

        attachLegalEntity.setId(rs.getLong("id_att_legal_entity"));
        attachLegalEntity.setCreateTime(rs.getTimestamp("create_time_att_legal_entity"));
        attachLegalEntity.setDocId(rs.getString("doc_id_att_legal_entity"));
        attachLegalEntity.setImgPath(rs.getString("img_path_att_legal_entity"));

        documentNewVender.setAttachBookBank(attachBookBank);
        documentNewVender.setAttachCompanySeal(attachCompanySeal);
        documentNewVender.setAttachHome(attachHome);
        documentNewVender.setAttachIdCard(attachIdCard);
        documentNewVender.setAttachLegalEntity(attachLegalEntity);
        DateTime dateOfBirth = new DateTime(documentNewVender.getBpBirthday());
        DateTime currentDate = new DateTime();
        Years diffInYears = Years.yearsBetween(dateOfBirth, currentDate);
        documentNewVender.setBdCountYear(String.valueOf(diffInYears.getYears()));
        return documentNewVender;
    }
}
