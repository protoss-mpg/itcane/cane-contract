package com.mpg.documentservice.mapper;

import com.mpg.documentservice.entity.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PromiseRowMapper implements RowMapper {
    @Override
    public DocumentPromise mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocumentPromise documentPromise = new DocumentPromise();
        AttachBookBank attachBookBank = new AttachBookBank();
        AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
        AttachHome attachHome = new AttachHome();
        AttachIdCard attachIdCard = new AttachIdCard();
        AttachLegalEntity attachLegalEntity = new AttachLegalEntity();

        documentPromise.setId(rs.getLong("id"));
        documentPromise.setDocId(rs.getString("doc_id"));
        documentPromise.setDocCreateDate(rs.getTimestamp("doc_create_date"));
        documentPromise.setDocCreateId(rs.getString("doc_create_id"));
        documentPromise.setDocCreateName(rs.getString("doc_create_name"));
        documentPromise.setDocCreatePosition(rs.getString("doc_create_position"));
        documentPromise.setDocDetail(rs.getString("doc_detail"));
        documentPromise.setDocFac(rs.getString("doc_fac"));
        documentPromise.setDocId(rs.getString("doc_id"));
        documentPromise.setDocImage(rs.getString("doc_image"));
        documentPromise.setDocStatus(rs.getString("doc_status"));
        documentPromise.setDocTypeId(rs.getString("doc_type_id"));
        documentPromise.setDocYear(rs.getString("doc_year"));
        documentPromise.setSignaturePersonalDate(rs.getTimestamp("signature_personal_date"));
        documentPromise.setSignaturePersonalName(rs.getString("signature_personal_name"));
        documentPromise.setSignaturePersonalPic(rs.getString("signature_personal_pic"));
        documentPromise.setSignaturePersonalPos(rs.getString("signature_personal_pos"));
        documentPromise.setSignatureFarmerName(rs.getString("signature_farmer_name"));
        documentPromise.setSignatureFarmerPic(rs.getString("signature_farmer_pic"));
        documentPromise.setSignatureFarmerPos(rs.getString("signature_farmer_pos"));
        documentPromise.setSignatureFarmerDate(rs.getTimestamp("signature_farmer_date"));
        documentPromise.setPathPdf(rs.getString("path_pdf"));
        documentPromise.setStatusPdf(rs.getString("status_pdf"));
        documentPromise.setBpCode(rs.getString("bp_code"));
        documentPromise.setBpCodeDoc(rs.getString("bp_code"));

        documentPromise.setSignatureDirectorDate(rs.getTimestamp("signature_director_date"));
        documentPromise.setSignatureDirectorId(rs.getString("signature_director_id"));

        documentPromise.setSignatureHeadzoneDate(rs.getTimestamp("signature_head_zone_date"));
        documentPromise.setSignatureHeadzoneId(rs.getString("signature_head_zone_id"));


        documentPromise.setSignatureDirectorFullName(rs.getString("name_director"));
        documentPromise.setSignatureDirectorPic(rs.getString("id_director"));

        documentPromise.setSignatureHeadzoneFullName(rs.getString("name_head_zone"));
        documentPromise.setSignatureHeadzonePic(rs.getString("id_head_zone"));

        documentPromise.setSignatureManagerFullName(rs.getString("name_manager"));
        documentPromise.setSignatureManagerPic(rs.getString("id_manager"));

        documentPromise.setStatusAppove(rs.getString("status_appove"));

        documentPromise.setZoneId(rs.getString("zone_id"));
        documentPromise.setSubZone(rs.getString("sub_zone"));

        documentPromise.setFacName(rs.getString("fac_name"));
        documentPromise.setFacHome(rs.getString("fac_home"));
        documentPromise.setFacVillageNo(rs.getString("fac_village_no"));
        documentPromise.setFacSubDistrict(rs.getString("fac_sub_district"));
        documentPromise.setFacDistrict(rs.getString("fac_district"));
        documentPromise.setFacProvince(rs.getString("fac_province"));
        documentPromise.setBranch(rs.getString("fac_branch"));
        documentPromise.setFacHeadPre(rs.getString("fac_head_pre"));
        documentPromise.setFacHeadName(rs.getString("fac_head_name"));
        documentPromise.setFacHeadLname(rs.getString("fac_head_lname"));

        documentPromise.setFacCode(rs.getString("fac_code"));
        documentPromise.setFacQueCode(rs.getString("fac_que_code"));

        documentPromise.setBpName(rs.getString("bp_name"));
        documentPromise.setDocDesc(rs.getString("doc_desc"));
        documentPromise.setTon(rs.getString("doc_ton"));
        documentPromise.setDocIdRef(rs.getString("doc_id_ref"));

        attachBookBank.setId(rs.getLong("id_att_book_bank"));
        attachBookBank.setCreateTime(rs.getTimestamp("create_time_att_book_bank"));
        attachBookBank.setDocId(rs.getString("doc_id_att_book_bank"));
        attachBookBank.setImgPathFront(rs.getString("img_path_front_att_book_bank"));
        attachBookBank.setImgPathBack(rs.getString("img_path_back_att_book_bank"));

        attachCompanySeal.setId(rs.getLong("id_att_company_seal"));
        attachCompanySeal.setCreateTime(rs.getTimestamp("create_time_att_company_seal"));
        attachCompanySeal.setDocId(rs.getString("doc_id_att_company_seal"));
        attachCompanySeal.setImgPath(rs.getString("img_path_att_company_seal"));

        attachHome.setId(rs.getLong("id_att_home"));
        attachHome.setCreateTime(rs.getTimestamp("create_time_att_home"));
        attachHome.setDocId(rs.getString("doc_id_att_home"));
        attachHome.setImgPathFront(rs.getString("img_path_front_att_home"));
        attachHome.setImgPathBack(rs.getString("img_path_back_att_home"));

        attachIdCard.setId(rs.getLong("id_att_idcard"));
        attachIdCard.setCreateTime(rs.getTimestamp("create_time_att_idcard"));
        attachIdCard.setDocId(rs.getString("doc_id_att_idcard"));
        attachIdCard.setImgPathFront(rs.getString("img_path_front_att_idcard"));
        attachIdCard.setImgPathBack(rs.getString("img_path_back_att_idcard"));

        attachLegalEntity.setId(rs.getLong("id_att_legal_entity"));
        attachLegalEntity.setCreateTime(rs.getTimestamp("create_time_att_legal_entity"));
        attachLegalEntity.setDocId(rs.getString("doc_id_att_legal_entity"));
        attachLegalEntity.setImgPath(rs.getString("img_path_att_legal_entity"));


        documentPromise.setAttachBookBank(attachBookBank);
        documentPromise.setAttachCompanySeal(attachCompanySeal);
        documentPromise.setAttachHome(attachHome);
        documentPromise.setAttachIdCard(attachIdCard);
        documentPromise.setAttachLegalEntity(attachLegalEntity);
        return documentPromise;
    }
}