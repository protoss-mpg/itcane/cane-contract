package com.mpg.documentservice.mapper;


import com.mpg.documentservice.entity.*;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewFarmerRowMapper implements RowMapper {
    @Override
    public DocumentNewFarmer mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocumentNewFarmer documentNewFarmer = new DocumentNewFarmer();
        AttachBookBank attachBookBank = new AttachBookBank();
        AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
        AttachHome attachHome = new AttachHome();
        AttachIdCard attachIdCard = new AttachIdCard();
        AttachLegalEntity attachLegalEntity = new AttachLegalEntity();

        documentNewFarmer.setId(rs.getLong("id"));
        documentNewFarmer.setDocId(rs.getString("doc_id"));
        documentNewFarmer.setDocCreateDate(rs.getTimestamp("doc_create_date"));
        documentNewFarmer.setDocCreateId(rs.getString("doc_create_id"));
        documentNewFarmer.setDocCreateName(rs.getString("doc_create_name"));
        documentNewFarmer.setDocCreatePosition(rs.getString("doc_create_position"));
        documentNewFarmer.setDocDetail(rs.getString("doc_detail"));
        documentNewFarmer.setDocFac(rs.getString("doc_fac"));
        documentNewFarmer.setDocId(rs.getString("doc_id"));
        documentNewFarmer.setDocImage(rs.getString("doc_image"));
        documentNewFarmer.setDocStatus(rs.getString("doc_status"));
        documentNewFarmer.setDocTypeId(rs.getString("doc_type_id"));
        documentNewFarmer.setDocYear(rs.getString("doc_year"));
        documentNewFarmer.setSignaturePersonalDate(rs.getTimestamp("signature_personal_date"));
        documentNewFarmer.setSignaturePersonalName(rs.getString("signature_personal_name"));
        documentNewFarmer.setSignaturePersonalPic(rs.getString("signature_personal_pic"));
        documentNewFarmer.setSignaturePersonalPos(rs.getString("signature_personal_pos"));
        documentNewFarmer.setSignatureFarmerName(rs.getString("signature_farmer_name"));
        documentNewFarmer.setSignatureFarmerPic(rs.getString("signature_farmer_pic"));
        documentNewFarmer.setSignatureFarmerPos(rs.getString("signature_farmer_pos"));
        documentNewFarmer.setSignatureFarmerDate(rs.getTimestamp("signature_farmer_date"));
        documentNewFarmer.setBpBirthday(rs.getString("bp_birthday"));
        documentNewFarmer.setBpCode(rs.getString("bp_code"));
        documentNewFarmer.setBpCodeDoc(rs.getString("bp_code"));
        documentNewFarmer.setBpDateCreate(rs.getTimestamp("bp_date_create"));
        documentNewFarmer.setBpDateUpdate(rs.getTimestamp("bp_date_update"));
        documentNewFarmer.setBpDistrict(rs.getString("bp_district"));
        documentNewFarmer.setBpHouseNo(rs.getString("bp_house_no"));
        documentNewFarmer.setBpHouseNumber(rs.getString("bp_house_number"));
        documentNewFarmer.setBpIdCard(rs.getString("bp_idcard"));
        documentNewFarmer.setBpIdCardEndDate(rs.getString("bp_idcard_end_date"));
        documentNewFarmer.setBpIdCardStartDate(rs.getString("bp_idcard_start_date"));
        documentNewFarmer.setBpImage(rs.getString("bp_image"));
        documentNewFarmer.setBpLname(rs.getString("bp_lname"));
        documentNewFarmer.setBpName(rs.getString("bp_name"));
        documentNewFarmer.setBpPre(rs.getString("bp_pre"));
        documentNewFarmer.setBpProvince(rs.getString("bp_province"));
        documentNewFarmer.setBpSpounseIdCard(rs.getString("bp_spouse_idcard"));
        documentNewFarmer.setBpSpouseLname(rs.getString("bp_spouse_lname"));
        documentNewFarmer.setBpSpouseName(rs.getString("bp_spouse_name"));


        documentNewFarmer.setBpSpousePre(rs.getString("bp_spouse_pre"));
        documentNewFarmer.setBpSpounseSta(rs.getString("bp_spouse_sta"));

        documentNewFarmer.setBpSubDistrict(rs.getString("bp_sub_district"));
        documentNewFarmer.setSignatureDirectorDate(rs.getTimestamp("signature_director_date"));
        documentNewFarmer.setSignatureDirectorId(rs.getString("signature_director_id"));
        documentNewFarmer.setSignatureHeadzoneDate(rs.getTimestamp("signature_head_zone_date"));
        documentNewFarmer.setSignatureHeadzoneId(rs.getString("signature_head_zone_id"));


        documentNewFarmer.setSignatureDirectorFullName(rs.getString("name_director"));
        documentNewFarmer.setSignatureDirectorPic(rs.getString("id_director"));

        documentNewFarmer.setSignatureHeadzoneFullName(rs.getString("name_head_zone"));
        documentNewFarmer.setSignatureHeadzonePic(rs.getString("id_head_zone"));

        documentNewFarmer.setSignatureManagerFullName(rs.getString("name_manager"));
        documentNewFarmer.setSignatureManagerPic(rs.getString("id_manager"));

        documentNewFarmer.setStatusAppove(rs.getString("status_appove"));

        documentNewFarmer.setZoneId(rs.getString("zone_id"));
        documentNewFarmer.setSubZone(rs.getString("sub_zone"));

        documentNewFarmer.setDocIdRef(rs.getString("doc_id_ref"));
        documentNewFarmer.setPathPdf(rs.getString("path_pdf"));
        documentNewFarmer.setStatusPdf(rs.getString("status_pdf"));

        documentNewFarmer.setFacName(rs.getString("fac_name"));
        documentNewFarmer.setFacHome(rs.getString("fac_home"));
        documentNewFarmer.setFacVillageNo(rs.getString("fac_village_no"));
        documentNewFarmer.setFacSubDistrict(rs.getString("fac_sub_district"));
        documentNewFarmer.setFacDistrict(rs.getString("fac_district"));
        documentNewFarmer.setFacProvince(rs.getString("fac_province"));
        documentNewFarmer.setBranch(rs.getString("fac_branch"));
        documentNewFarmer.setFacHeadPre(rs.getString("fac_head_pre"));
        documentNewFarmer.setFacHeadName(rs.getString("fac_head_name"));
        documentNewFarmer.setFacHeadLname(rs.getString("fac_head_lname"));

        documentNewFarmer.setFacCode(rs.getString("fac_code"));
        documentNewFarmer.setFacQueCode(rs.getString("fac_que_code"));

        documentNewFarmer.setBpEmail(rs.getString("bp_email"));
        documentNewFarmer.setBpPhone(rs.getString("bp_phone"));

        attachBookBank.setId(rs.getLong("id_att_book_bank"));
        attachBookBank.setCreateTime(rs.getTimestamp("create_time_att_book_bank"));
        attachBookBank.setDocId(rs.getString("doc_id_att_book_bank"));
        attachBookBank.setImgPathFront(rs.getString("img_path_front_att_book_bank"));
        attachBookBank.setImgPathBack(rs.getString("img_path_back_att_book_bank"));

        attachCompanySeal.setId(rs.getLong("id_att_company_seal"));
        attachCompanySeal.setCreateTime(rs.getTimestamp("create_time_att_company_seal"));
        attachCompanySeal.setDocId(rs.getString("doc_id_att_company_seal"));
        attachCompanySeal.setImgPath(rs.getString("img_path_att_company_seal"));

        attachHome.setId(rs.getLong("id_att_home"));
        attachHome.setCreateTime(rs.getTimestamp("create_time_att_home"));
        attachHome.setDocId(rs.getString("doc_id_att_home"));
        attachHome.setImgPathFront(rs.getString("img_path_front_att_home"));
        attachHome.setImgPathBack(rs.getString("img_path_back_att_home"));

        attachIdCard.setId(rs.getLong("id_att_idcard"));
        attachIdCard.setCreateTime(rs.getTimestamp("create_time_att_idcard"));
        attachIdCard.setDocId(rs.getString("doc_id_att_idcard"));
        attachIdCard.setImgPathFront(rs.getString("img_path_front_att_idcard"));
        attachIdCard.setImgPathBack(rs.getString("img_path_back_att_idcard"));

        attachLegalEntity.setId(rs.getLong("id_att_legal_entity"));
        attachLegalEntity.setCreateTime(rs.getTimestamp("create_time_att_legal_entity"));
        attachLegalEntity.setDocId(rs.getString("doc_id_att_legal_entity"));
        attachLegalEntity.setImgPath(rs.getString("img_path_att_legal_entity"));

        documentNewFarmer.setAttachBookBank(attachBookBank);
        documentNewFarmer.setAttachCompanySeal(attachCompanySeal);
        documentNewFarmer.setAttachHome(attachHome);
        documentNewFarmer.setAttachIdCard(attachIdCard);
        documentNewFarmer.setAttachLegalEntity(attachLegalEntity);

        DateTime dateOfBirth = new DateTime(documentNewFarmer.getBpBirthday());
        DateTime currentDate = new DateTime();
        Years diffInYears = Years.yearsBetween(dateOfBirth, currentDate);
        documentNewFarmer.setBdCountYear(String.valueOf(diffInYears.getYears()));
        return documentNewFarmer;
    }
}
