package com.mpg.documentservice.mapper;

import com.mpg.documentservice.entity.*;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FarmerRowMapper implements RowMapper {


    @Override
    public DocumentFarmer mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocumentFarmer documentFarmer = new DocumentFarmer();
        AttachBookBank attachBookBank = new AttachBookBank();
        AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
        AttachHome attachHome = new AttachHome();
        AttachIdCard attachIdCard = new AttachIdCard();
        AttachLegalEntity attachLegalEntity = new AttachLegalEntity();

        documentFarmer.setId(rs.getLong("id"));
        documentFarmer.setDocId(rs.getString("doc_id"));
        documentFarmer.setDocCreateDate(rs.getTimestamp("doc_create_date"));
        documentFarmer.setDocCreateId(rs.getString("doc_create_id"));
        documentFarmer.setDocCreateName(rs.getString("doc_create_name"));
        documentFarmer.setDocCreatePosition(rs.getString("doc_create_position"));
        documentFarmer.setDocDetail(rs.getString("doc_detail"));
        documentFarmer.setDocFac(rs.getString("doc_fac"));
        documentFarmer.setDocId(rs.getString("doc_id"));
        documentFarmer.setDocImage(rs.getString("doc_image"));
        documentFarmer.setDocStatus(rs.getString("doc_status"));
        documentFarmer.setDocTypeId(rs.getString("doc_type_id"));
        documentFarmer.setDocYear(rs.getString("doc_year"));
        documentFarmer.setSignaturePersonalDate(rs.getTimestamp("signature_personal_date"));
        documentFarmer.setSignaturePersonalName(rs.getString("signature_personal_name"));
        documentFarmer.setSignaturePersonalPic(rs.getString("signature_personal_pic"));
        documentFarmer.setSignaturePersonalPos(rs.getString("signature_personal_pos"));
        documentFarmer.setSignatureFarmerName(rs.getString("signature_farmer_name"));
        documentFarmer.setSignatureFarmerPic(rs.getString("signature_farmer_pic"));
        documentFarmer.setSignatureFarmerPos(rs.getString("signature_farmer_pos"));
        documentFarmer.setSignatureFarmerDate(rs.getTimestamp("signature_farmer_date"));
        documentFarmer.setBpBirthday(rs.getString("bp_birthday"));
        documentFarmer.setBpCode(rs.getString("bp_code"));
        documentFarmer.setBpCodeDoc(rs.getString("bp_code"));
        documentFarmer.setBpDateCreate(rs.getTimestamp("bp_date_create"));
        documentFarmer.setBpDateUpdate(rs.getTimestamp("bp_date_update"));
        documentFarmer.setBpDistrict(rs.getString("bp_district"));
        documentFarmer.setBpHouseNo(rs.getString("bp_house_no"));
        documentFarmer.setBpHouseNumber(rs.getString("bp_house_number"));
        documentFarmer.setBpIdCard(rs.getString("bp_idcard"));
        documentFarmer.setBpIdCardEndDate(rs.getString("bp_idcard_end_date"));
        documentFarmer.setBpIdCardStartDate(rs.getString("bp_idcard_start_date"));
        documentFarmer.setBpImage(rs.getString("bp_image"));
        documentFarmer.setBpLname(rs.getString("bp_lname"));
        documentFarmer.setBpName(rs.getString("bp_name"));
        documentFarmer.setBpPre(rs.getString("bp_pre"));
        documentFarmer.setBpProvince(rs.getString("bp_province"));
        documentFarmer.setBpSpounseIdCard(rs.getString("bp_spouse_idcard"));
        documentFarmer.setBpSpouseLname(rs.getString("bp_spouse_lname"));
        documentFarmer.setBpSpouseName(rs.getString("bp_spouse_name"));
        documentFarmer.setBpSpousePre(rs.getString("bp_spouse_pre"));
        documentFarmer.setBpSpounseSta(rs.getString("bp_spouse_sta"));
        documentFarmer.setBpSubDistrict(rs.getString("bp_sub_district"));
        documentFarmer.setSignatureDirectorDate(rs.getTimestamp("signature_director_date"));
        documentFarmer.setSignatureDirectorId(rs.getString("signature_director_id"));
        documentFarmer.setSignatureHeadzoneDate(rs.getTimestamp("signature_head_zone_date"));
        documentFarmer.setSignatureHeadzoneId(rs.getString("signature_head_zone_id"));
        documentFarmer.setZoneId(rs.getString("zone_id"));
        documentFarmer.setStatusAppove(rs.getString("status_appove"));
        documentFarmer.setSubZone(rs.getString("sub_zone"));
        documentFarmer.setSignatureDirectorFullName(rs.getString("name_director"));
        documentFarmer.setSignatureDirectorPic(rs.getString("id_director"));

        documentFarmer.setSignatureHeadzoneFullName(rs.getString("name_head_zone"));
        documentFarmer.setSignatureHeadzonePic(rs.getString("id_head_zone"));
        documentFarmer.setSignatureManagerFullName(rs.getString("name_manager"));
        documentFarmer.setSignatureManagerPic(rs.getString("id_manager"));


        documentFarmer.setDocIdRef(rs.getString("doc_id_ref"));

        documentFarmer.setPathPdf(rs.getString("path_pdf"));
        documentFarmer.setStatusPdf(rs.getString("status_pdf"));

        documentFarmer.setBpEmail(rs.getString("bp_email"));
        documentFarmer.setBpPhone(rs.getString("bp_phone"));

        documentFarmer.setFacName(rs.getString("fac_name"));
        documentFarmer.setFacHome(rs.getString("fac_home"));
        documentFarmer.setFacVillageNo(rs.getString("fac_village_no"));
        documentFarmer.setFacSubDistrict(rs.getString("fac_sub_district"));
        documentFarmer.setFacDistrict(rs.getString("fac_district"));
        documentFarmer.setFacProvince(rs.getString("fac_province"));
        documentFarmer.setBranch(rs.getString("fac_branch"));
        documentFarmer.setFacHeadPre(rs.getString("fac_head_pre"));
        documentFarmer.setFacHeadName(rs.getString("fac_head_name"));
        documentFarmer.setFacHeadLname(rs.getString("fac_head_lname"));

        documentFarmer.setFacCode(rs.getString("fac_code"));
        documentFarmer.setFacQueCode(rs.getString("fac_que_code"));

        attachBookBank.setId(rs.getLong("id_att_book_bank"));
        attachBookBank.setCreateTime(rs.getTimestamp("create_time_att_book_bank"));
        attachBookBank.setDocId(rs.getString("doc_id_att_book_bank"));
        attachBookBank.setImgPathFront(rs.getString("img_path_front_att_book_bank"));
        attachBookBank.setImgPathBack(rs.getString("img_path_back_att_book_bank"));

        attachCompanySeal.setId(rs.getLong("id_att_company_seal"));
        attachCompanySeal.setCreateTime(rs.getTimestamp("create_time_att_company_seal"));
        attachCompanySeal.setDocId(rs.getString("doc_id_att_company_seal"));
        attachCompanySeal.setImgPath(rs.getString("img_path_att_company_seal"));

        attachHome.setId(rs.getLong("id_att_home"));
        attachHome.setCreateTime(rs.getTimestamp("create_time_att_home"));
        attachHome.setDocId(rs.getString("doc_id_att_home"));
        attachHome.setImgPathFront(rs.getString("img_path_front_att_home"));
        attachHome.setImgPathBack(rs.getString("img_path_back_att_home"));

        attachIdCard.setId(rs.getLong("id_att_idcard"));
        attachIdCard.setCreateTime(rs.getTimestamp("create_time_att_idcard"));
        attachIdCard.setDocId(rs.getString("doc_id_att_idcard"));
        attachIdCard.setImgPathFront(rs.getString("img_path_front_att_idcard"));
        attachIdCard.setImgPathBack(rs.getString("img_path_back_att_idcard"));

        attachLegalEntity.setId(rs.getLong("id_att_legal_entity"));
        attachLegalEntity.setCreateTime(rs.getTimestamp("create_time_att_legal_entity"));
        attachLegalEntity.setDocId(rs.getString("doc_id_att_legal_entity"));
        attachLegalEntity.setImgPath(rs.getString("img_path_att_legal_entity"));

        documentFarmer.setAttachBookBank(attachBookBank);
        documentFarmer.setAttachCompanySeal(attachCompanySeal);
        documentFarmer.setAttachHome(attachHome);
        documentFarmer.setAttachIdCard(attachIdCard);
        documentFarmer.setAttachLegalEntity(attachLegalEntity);
        DateTime dateOfBirth = new DateTime(documentFarmer.getBpBirthday());
        DateTime currentDate = new DateTime();
        Years diffInYears = Years.yearsBetween(dateOfBirth, currentDate);
        documentFarmer.setBdCountYear(String.valueOf(diffInYears.getYears()));
        return documentFarmer;
    }
}
