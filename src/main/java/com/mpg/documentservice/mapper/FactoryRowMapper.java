package com.mpg.documentservice.mapper;

import com.mpg.documentservice.entity.FactoryConfig;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FactoryRowMapper implements RowMapper {
    @Override
    public FactoryConfig mapRow(ResultSet rs, int rowNum) throws SQLException {
        FactoryConfig factoryConfig = new FactoryConfig();
        factoryConfig.setId(rs.getLong("id"));
        factoryConfig.setBranch(rs.getString("fac_branch"));
        factoryConfig.setFac(rs.getString("fac"));
        factoryConfig.setFacCode(rs.getString("fac_code"));
        factoryConfig.setFacDistrict(rs.getString("fac_district"));
        factoryConfig.setFacHeadLname(rs.getString("fac_head_lname"));
        factoryConfig.setFacHeadName(rs.getString("fac_head_name"));
        factoryConfig.setFacHeadPre(rs.getString("fac_head_pre"));
        factoryConfig.setFacHome(rs.getString("fac_home"));
        factoryConfig.setFacHeadName(rs.getString("fac_name"));

        factoryConfig.setFacProvince(rs.getString("fac_province"));
        factoryConfig.setFacQueCode(rs.getString("fac_que_code"));
        factoryConfig.setFacSubDistrict(rs.getString("fac_sub_district"));
        factoryConfig.setFacVillageNo(rs.getString("fac_village_no"));

        return factoryConfig;
    }
}
