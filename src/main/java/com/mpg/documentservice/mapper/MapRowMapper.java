package com.mpg.documentservice.mapper;

import com.mpg.documentservice.entity.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MapRowMapper implements RowMapper {
    @Override
    public DocumentMap mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocumentMap documentMap = new DocumentMap();
        AttachBookBank attachBookBank = new AttachBookBank();
        AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
        AttachHome attachHome = new AttachHome();
        AttachIdCard attachIdCard = new AttachIdCard();
        AttachLegalEntity attachLegalEntity = new AttachLegalEntity();

        documentMap.setId(rs.getLong("id"));
        documentMap.setDocId(rs.getString("doc_id"));
        documentMap.setDocCreateDate(rs.getTimestamp("doc_create_date"));
        documentMap.setDocCreateId(rs.getString("doc_create_id"));
        documentMap.setDocCreateName(rs.getString("doc_create_name"));
        documentMap.setDocCreatePosition(rs.getString("doc_create_position"));
        documentMap.setDocDetail(rs.getString("doc_detail"));
        documentMap.setDocFac(rs.getString("doc_fac"));
        documentMap.setDocId(rs.getString("doc_id"));
        documentMap.setDocImage(rs.getString("doc_image"));
        documentMap.setDocStatus(rs.getString("doc_status"));
        documentMap.setDocTypeId(rs.getString("doc_type_id"));
        documentMap.setDocYear(rs.getString("doc_year"));
        documentMap.setSignaturePersonalDate(rs.getTimestamp("signature_personal_date"));
        documentMap.setSignaturePersonalName(rs.getString("signature_personal_name"));
        documentMap.setSignaturePersonalPic(rs.getString("signature_personal_pic"));
        documentMap.setSignaturePersonalPos(rs.getString("signature_personal_pos"));
        documentMap.setSignatureFarmerName(rs.getString("signature_farmer_name"));
        documentMap.setSignatureFarmerPic(rs.getString("signature_farmer_pic"));
        documentMap.setSignatureFarmerPos(rs.getString("signature_farmer_pos"));
        documentMap.setSignatureFarmerDate(rs.getTimestamp("signature_farmer_date"));

        documentMap.setAmpName(rs.getString("amp_name"));
        documentMap.setBanName(rs.getString("ban_name"));
        documentMap.setBpCodeDoc(rs.getString("bp_code"));
        documentMap.setBpIdCard(rs.getString("bp_idcard"));
        documentMap.setBpName(rs.getString("bp_name"));
        documentMap.setCaneAcId(rs.getString("cane_ac_id"));
        documentMap.setCaneTypeName(rs.getString("cane_type_name"));
        documentMap.setCaseCaneName(rs.getString("case_cane_name"));
        documentMap.setCheckType(rs.getString("check_type"));
        documentMap.setCropYear(rs.getString("crop_year"));
        documentMap.setDocDesc(rs.getString("doc_desc"));
        documentMap.setDocPdf(rs.getString("doc_pdf"));
        documentMap.setFId(rs.getString("f_id"));
        documentMap.setGisArea(rs.getString("gis_area"));
        documentMap.setInputFid(rs.getString("input_fid"));
        documentMap.setPathFileAct(rs.getString("path_file_act"));
        documentMap.setPathFileShp(rs.getString("path_file_shp"));
        documentMap.setPrvName(rs.getString("prv_name"));
        documentMap.setSignatureHeadzoneDate(rs.getTimestamp("signature_head_zone_date"));
        documentMap.setSignatureHeadzoneId(rs.getString("signature_head_zone_id"));
        documentMap.setSignatureManagerDate(rs.getTimestamp("signature_manager_date"));
        documentMap.setSignatureManagerId(rs.getString("signature_manager_id"));

        documentMap.setSignatureDirectorFullName(rs.getString("name_director"));
        documentMap.setSignatureDirectorPic(rs.getString("id_director"));

        documentMap.setSignatureHeadzoneFullName(rs.getString("name_head_zone"));
        documentMap.setSignatureHeadzonePic(rs.getString("id_head_zone"));

        documentMap.setSignatureManagerFullName(rs.getString("name_manager"));
        documentMap.setSignatureManagerPic(rs.getString("id_manager"));

        documentMap.setSubZone(rs.getString("sub_zone"));
        documentMap.setTamName(rs.getString("tam_name"));
        documentMap.setTypeOwnerFull(rs.getString("type_owner_full"));
        documentMap.setTypeOwnerName(rs.getString("type_owner_name"));
        documentMap.setZoneId(rs.getString("zone_id"));
        documentMap.setDocIdRef(rs.getString("doc_id_ref"));
        documentMap.setPathPdf(rs.getString("path_pdf"));
        documentMap.setStatusPdf(rs.getString("status_pdf"));
        documentMap.setStatusAppove(rs.getString("status_appove"));
        documentMap.setFacName(rs.getString("fac_name"));
        documentMap.setFacHome(rs.getString("fac_home"));
        documentMap.setFacVillageNo(rs.getString("fac_village_no"));
        documentMap.setFacSubDistrict(rs.getString("fac_sub_district"));
        documentMap.setFacDistrict(rs.getString("fac_district"));
        documentMap.setFacProvince(rs.getString("fac_province"));
        documentMap.setBranch(rs.getString("fac_branch"));
        documentMap.setFacHeadPre(rs.getString("fac_head_pre"));
        documentMap.setFacHeadName(rs.getString("fac_head_name"));
        documentMap.setFacHeadLname(rs.getString("fac_head_lname"));
        documentMap.setFacCode(rs.getString("fac_code"));
        documentMap.setFacQueCode(rs.getString("fac_que_code"));

        attachBookBank.setId(rs.getLong("id_att_book_bank"));
        attachBookBank.setCreateTime(rs.getTimestamp("create_time_att_book_bank"));
        attachBookBank.setDocId(rs.getString("doc_id_att_book_bank"));
        attachBookBank.setImgPathFront(rs.getString("img_path_front_att_book_bank"));
        attachBookBank.setImgPathBack(rs.getString("img_path_back_att_book_bank"));

        attachCompanySeal.setId(rs.getLong("id_att_company_seal"));
        attachCompanySeal.setCreateTime(rs.getTimestamp("create_time_att_company_seal"));
        attachCompanySeal.setDocId(rs.getString("doc_id_att_company_seal"));
        attachCompanySeal.setImgPath(rs.getString("img_path_att_company_seal"));

        attachHome.setId(rs.getLong("id_att_home"));
        attachHome.setCreateTime(rs.getTimestamp("create_time_att_home"));
        attachHome.setDocId(rs.getString("doc_id_att_home"));
        attachHome.setImgPathFront(rs.getString("img_path_front_att_home"));
        attachHome.setImgPathBack(rs.getString("img_path_back_att_home"));

        attachIdCard.setId(rs.getLong("id_att_idcard"));
        attachIdCard.setCreateTime(rs.getTimestamp("create_time_att_idcard"));
        attachIdCard.setDocId(rs.getString("doc_id_att_idcard"));
        attachIdCard.setImgPathFront(rs.getString("img_path_front_att_idcard"));
        attachIdCard.setImgPathBack(rs.getString("img_path_back_att_idcard"));

        attachLegalEntity.setId(rs.getLong("id_att_legal_entity"));
        attachLegalEntity.setCreateTime(rs.getTimestamp("create_time_att_legal_entity"));
        attachLegalEntity.setDocId(rs.getString("doc_id_att_legal_entity"));
        attachLegalEntity.setImgPath(rs.getString("img_path_att_legal_entity"));

        documentMap.setAttachBookBank(attachBookBank);
        documentMap.setAttachCompanySeal(attachCompanySeal);
        documentMap.setAttachHome(attachHome);
        documentMap.setAttachIdCard(attachIdCard);
        documentMap.setAttachLegalEntity(attachLegalEntity);
        return documentMap;
    }
}
