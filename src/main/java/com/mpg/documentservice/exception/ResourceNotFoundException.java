package com.mpg.documentservice.exception;


import java.util.Date;

public class ResourceNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;
    private Integer code;
    private Date dateMsg;
    private String typeMsg;
    private String generalMsg;
    private String technicalMsg;

    public ResourceNotFoundException(Integer code, Date dateMsg, String typeMsg, String generalMsg, String technicalMsg) {
        super(generalMsg);
        this.code = code;
        this.dateMsg = dateMsg;
        this.typeMsg = typeMsg;
        this.generalMsg = generalMsg;
        this.technicalMsg = technicalMsg;
    }
}

