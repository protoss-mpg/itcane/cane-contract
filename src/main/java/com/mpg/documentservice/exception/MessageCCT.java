package com.mpg.documentservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@ToString

public class MessageCCT {
    private Integer code;
    private Date dateMsg;
    private String typeMsg;
    private String generalMsg;
    private String technicalMsg;


}
