/**
 * 
 */
/** 
 * @author suriya_e@protosstechnology.com
 *
 */
package com.mpg.documentservice.model;


import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;


@Data
public class ResponseModel {
	
	
	private Boolean success;
	private String message;
	@JsonInclude(Include.NON_NULL)
	private Object data;
	
	public ResponseModel() {
		setSuccess(false);
        setMessage("Process Fail");
        setData(new HashMap());
	}

	
}
