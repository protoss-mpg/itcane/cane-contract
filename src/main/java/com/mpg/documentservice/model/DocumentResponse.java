package com.mpg.documentservice.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DocumentResponse implements Serializable {
    private String statusMsg;
    private String codeTypeMsg;
    private String msg;
    private String docId;
    private Long dataId;
}
