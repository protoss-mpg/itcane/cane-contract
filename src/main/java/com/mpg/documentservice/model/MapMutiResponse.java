package com.mpg.documentservice.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class MapMutiResponse implements Serializable {

    private String statusMsg;
    private String codeTypeMsg;
    private String msg;

    private List docId;
    private List dataId;
    private String caneAcId;
}
