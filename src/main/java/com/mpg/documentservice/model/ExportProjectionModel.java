package com.mpg.documentservice.model;

import lombok.Data;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class ExportProjectionModel {

	private Integer indexDisplay;
	private String  columnAlign;
	
	
	
}
