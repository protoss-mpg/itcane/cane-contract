package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentFarmer;
import com.mpg.documentservice.mapper.FarmerRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Repository
public class DocumentFarmerJDBCRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Transactional
    public DocumentFarmer selectFarmerByDocId(String docId, String fac, String year) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("docId", docId);
        parameters.put("fac", fac);
        parameters.put("year", year);

        final String SELECTFARMMERBYDOCID = "select top(1) d.id " +
                "      ,d.bp_code " +
                "      ,d.doc_create_date " +
                "      ,d.doc_create_id " +
                "      ,d.doc_create_name " +
                "      ,d.doc_create_position " +
                "      ,d.doc_detail " +
                "      ,d.doc_fac " +
                "      ,d.doc_id " +
                "      ,d.doc_image " +
                "      ,d.doc_status " +
                "      ,d.doc_type_id " +
                "      ,d.doc_year " +
                "      ,d.signature_personal_date " +
                "      ,d.signature_personal_name " +
                "      ,d.signature_personal_pic " +
                "      ,d.signature_personal_pos " +
                "      ,d.signature_farmer_name " +
                "      ,d.signature_farmer_pic " +
                "      ,d.signature_farmer_pos " +
                "      ,d.signature_farmer_date " +
                "      ,d.zone_id " +
                "      ,d.sub_zone " +
                "      ,d.path_pdf " +
                "      ,d.status_pdf " +
                "      ,d.status_appove " +
                "      ,f.bp_birthday " +
                "      ,f.bp_code " +
                "      ,f.bp_date_create " +
                "      ,f.bp_date_update " +
                "      ,f.bp_district " +
                "      ,f.bp_house_no " +
                "      ,f.bp_house_number " +
                "      ,f.bp_idcard " +
                "      ,f.bp_idcard_end_date " +
                "      ,f.bp_idcard_start_date " +
                "      ,f.bp_image " +
                "      ,f.bp_lname " +
                "      ,f.bp_name " +
                "      ,f.bp_pre " +
                "      ,f.bp_province " +
                "      ,f.bp_spouse_idcard " +
                "      ,f.bp_spouse_lname " +
                "      ,f.bp_spouse_name " +
                "      ,f.bp_spouse_pre " +
                "      ,f.bp_spouse_sta " +
                "      ,f.bp_sub_district " +
                "      ,d.signature_director_date " +
                "      ,d.signature_director_id " +
                "      ,d.signature_head_zone_date " +
                "      ,d.signature_head_zone_id " +
                "      ,f.doc_id_ref " +
                "      ,f.bp_phone " +
                "      ,f.bp_email " +
                "      ,b.id as id_att_book_bank" +
                "      ,b.create_time as create_time_att_book_bank" +
                "      ,b.doc_id as doc_id_att_book_bank" +
                "      ,b.img_path_front as  img_path_front_att_book_bank" +
                "      ,b.img_path_back as  img_path_back_att_book_bank" +
                "      ,c.id as id_att_company_seal" +
                "      ,c.create_time as create_time_att_company_seal" +
                "      ,c.doc_id as doc_id_att_company_seal" +
                "      ,c.img_path as img_path_att_company_seal" +
                "      ,h.id as id_att_home" +
                "      ,h.create_time as create_time_att_home" +
                "      ,h.doc_id as doc_id_att_home" +
                "      ,h.img_path_front as img_path_front_att_home" +
                "      ,h.img_path_back as img_path_back_att_home" +
                "      ,i.id as id_att_idcard" +
                "      ,i.create_time as create_time_att_idcard" +
                "      ,i.doc_id as doc_id_att_idcard" +
                "      ,i.img_path_front as img_path_front_att_idcard" +
                "      ,i.img_path_back as img_path_back_att_idcard" +
                "      ,l.id as id_att_legal_entity" +
                "      ,l.create_time as create_time_att_legal_entity" +
                "      ,l.doc_id as doc_id_att_legal_entity" +
                "      ,l.img_path as img_path_att_legal_entity" +
                ",fa.fac_branch \n" +
                ",fa.fac\n" +
                ",fa.fac_district\n" +
                ",fa.fac_head_lname\n" +
                ",fa.fac_head_name\n" +
                ",fa.fac_head_pre\n" +
                ",fa.fac_home\n" +
                ",fa.fac_name\n" +
                ",fa.fac_province\n" +
                ",fa.fac_sub_district\n" +
                ",fa.fac_village_no"+
                ",fa.fac_code"+
                ",fa.fac_que_code"+
                ",si.full_name as name_head_zone"+
                ",si.signature as id_head_zone"+
                ",si1.full_name as name_director"+
                ",si1.signature as id_director"+
                ",si2.full_name as name_manager"+
                ",si2.signature as id_manager"+
                "   from  " +
                "tbl_document d  " +
                "join tbl_document_farmer f on d.id=f.doc_id_ref " +
                "left join tbl_att_book_bank b on  b.id=d.att_book_bank_id " +
                "left join tbl_att_company_seal c on  c.id=d.att_company_seal_id " +
                "left join tbl_att_home h on  h.id=d.att_home_id " +
                "left join tbl_att_idcard i on  i.id=d.att_idcard_id " +
                "left join tbl_att_legal_entity l on  l.id=d.att_legal_entity_id " +
                "left join tbl_factory fa  on  fa.fac=d.doc_fac " +
                "left join tbl_signature si  on  si.user_id=d.signature_head_zone_id  " +
                "left join tbl_signature si1  on  si1.user_id=d.signature_director_id  " +
                "left join tbl_signature si2  on  si2.user_id=d.signature_manager_id  " +
                "where d.doc_fac = :fac and d.doc_id = :docId and d.doc_year = :year;";
        return (DocumentFarmer) namedParameterJdbcTemplate.queryForObject(SELECTFARMMERBYDOCID, parameters, new FarmerRowMapper());
    }
    @Transactional
    public DocumentFarmer selectFarmerById(Long id) {
        Map<String, Long> parameters = new HashMap<>();
        parameters.put("id", id);
        final String SELECTFARMMERBYID = "select top(1) d.id " +
                "      ,d.bp_code " +
                "      ,d.doc_create_date " +
                "      ,d.doc_create_id " +
                "      ,d.doc_create_name " +
                "      ,d.doc_create_position " +
                "      ,d.doc_detail " +
                "      ,d.doc_fac " +
                "      ,d.doc_id " +
                "      ,d.doc_image " +
                "      ,d.doc_status " +
                "      ,d.doc_type_id " +
                "      ,d.doc_year " +
                "      ,d.signature_personal_date " +
                "      ,d.signature_personal_name " +
                "      ,d.signature_personal_pic " +
                "      ,d.signature_personal_pos " +
                "      ,d.signature_farmer_name " +
                "      ,d.signature_farmer_pic " +
                "      ,d.signature_farmer_pos " +
                "      ,d.signature_farmer_date " +
                "      ,d.zone_id " +
                "      ,d.sub_zone " +
                "      ,d.path_pdf " +
                "      ,d.status_appove " +
                "      ,d.status_pdf " +
                "      ,f.bp_birthday " +
                "      ,f.bp_code " +
                "      ,f.bp_date_create " +
                "      ,f.bp_date_update " +
                "      ,f.bp_district " +
                "      ,f.bp_house_no " +
                "      ,f.bp_house_number " +
                "      ,f.bp_idcard " +
                "      ,f.bp_idcard_end_date " +
                "      ,f.bp_idcard_start_date " +
                "      ,f.bp_image " +
                "      ,f.bp_lname " +
                "      ,f.bp_name " +
                "      ,f.bp_pre " +
                "      ,f.bp_province " +
                "      ,f.bp_spouse_idcard " +
                "      ,f.bp_spouse_lname " +
                "      ,f.bp_spouse_name " +
                "      ,f.bp_spouse_pre " +
                "      ,f.bp_spouse_sta " +
                "      ,f.bp_sub_district " +
                "      ,d.signature_director_date " +
                "      ,d.signature_director_id " +
                "      ,d.signature_head_zone_date " +
                "      ,d.signature_head_zone_id " +
                "      ,f.doc_id_ref " +
                "      ,f.bp_phone " +
                "      ,f.bp_email " +
                "      ,b.id as id_att_book_bank" +
                "      ,b.create_time as create_time_att_book_bank" +
                "      ,b.doc_id as doc_id_att_book_bank" +
                "      ,b.img_path_front as  img_path_front_att_book_bank" +
                "      ,b.img_path_back as  img_path_back_att_book_bank" +
                "      ,c.id as id_att_company_seal" +
                "      ,c.create_time as create_time_att_company_seal" +
                "      ,c.doc_id as doc_id_att_company_seal" +
                "      ,c.img_path as img_path_att_company_seal" +
                "      ,h.id as id_att_home" +
                "      ,h.create_time as create_time_att_home" +
                "      ,h.doc_id as doc_id_att_home" +
                "      ,h.img_path_front as img_path_front_att_home" +
                "      ,h.img_path_back as img_path_back_att_home" +
                "      ,i.id as id_att_idcard" +
                "      ,i.create_time as create_time_att_idcard" +
                "      ,i.doc_id as doc_id_att_idcard" +
                "      ,i.img_path_front as img_path_front_att_idcard" +
                "      ,i.img_path_back as img_path_back_att_idcard" +
                "      ,l.id as id_att_legal_entity" +
                "      ,l.create_time as create_time_att_legal_entity" +
                "      ,l.doc_id as doc_id_att_legal_entity" +
                "      ,l.img_path as img_path_att_legal_entity" +
                ",fa.fac_branch \n" +
                ",fa.fac\n" +
                ",fa.fac_district\n" +
                ",fa.fac_head_lname\n" +
                ",fa.fac_head_name\n" +
                ",fa.fac_head_pre\n" +
                ",fa.fac_home\n" +
                ",fa.fac_name\n" +
                ",fa.fac_province\n" +
                ",fa.fac_sub_district\n" +
                ",fa.fac_village_no"+
                ",fa.fac_code"+
                ",fa.fac_que_code"+
                ",si.full_name as name_head_zone"+
                ",si.signature as id_head_zone"+
                ",si1.full_name as name_director"+
                ",si1.signature as id_director"+
                ",si2.full_name as name_manager"+
                ",si2.signature as id_manager"+
                "   from  " +
                "tbl_document d  " +
                "join tbl_document_farmer f on d.id=f.doc_id_ref " +
                "left join tbl_att_book_bank b on  b.id=d.att_book_bank_id " +
                "left join tbl_att_company_seal c on  c.id=d.att_company_seal_id " +
                "left join tbl_att_home h on  h.id=d.att_home_id " +
                "left join tbl_att_idcard i on  i.id=d.att_idcard_id " +
                "left join tbl_att_legal_entity l on  l.id=d.att_legal_entity_id " +
                "left join tbl_factory fa  on  fa.fac=d.doc_fac " +
                "left join tbl_signature si  on  si.user_id=d.signature_head_zone_id  " +
                "left join tbl_signature si1  on  si1.user_id=d.signature_director_id  " +
                "left join tbl_signature si2  on  si2.user_id=d.signature_manager_id  " +
                "where d.id = :id;" ;
        return (DocumentFarmer) namedParameterJdbcTemplate.queryForObject(SELECTFARMMERBYID, parameters, new FarmerRowMapper());
    }
}
