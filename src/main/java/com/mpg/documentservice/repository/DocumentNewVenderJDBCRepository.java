package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentNewVender;
import com.mpg.documentservice.mapper.NewVenderRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Repository
public class DocumentNewVenderJDBCRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional
    public DocumentNewVender selectNewVenderByDocId(String docId, String fac, String year) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("docId", docId);
        parameters.put("fac", fac);
        parameters.put("year", year);
        final String SELECTNEWVEBDERBYDOCID ="select top(1) d.id  \n" +
                "                ,d.bp_code  \n" +
                "                ,d.doc_create_date  \n" +
                "                ,d.doc_create_id  \n" +
                "                ,d.doc_create_name  \n" +
                "                ,d.doc_create_position  \n" +
                "                ,d.doc_detail  \n" +
                "                ,d.doc_fac  \n" +
                "                ,d.doc_id  \n" +
                "                ,d.doc_image  \n" +
                "                ,d.doc_status  \n" +
                "                ,d.doc_type_id  \n" +
                "                ,d.doc_year  \n" +
                "                ,d.signature_personal_date  \n" +
                "                ,d.signature_personal_name  \n" +
                "                ,d.signature_personal_pic  \n" +
                "                ,d.signature_personal_pos  \n" +
                "                ,d.signature_farmer_name  \n" +
                "                ,d.signature_farmer_pic  \n" +
                "                ,d.signature_farmer_pos  \n" +
                "                ,d.signature_farmer_date  \n" +
                "      ,d.path_pdf " +
                "      ,d.status_pdf " +
                "                ,v.bp_birthday\n" +
                "                ,v.bp_code\n" +
                "                ,v.bp_company_date\n" +
                "                ,v.bp_company_idcard\n" +
                "                ,v.bp_company_name\n" +
                "                ,v.bp_company_sta\n" +
                "                ,v.bp_date_create\n" +
                "                ,v.bp_date_update\n" +
                "                ,v.bp_district\n" +
                "                ,v.bp_email\n" +
                "                ,v.bp_full_name\n" +
                "                ,v.bp_house_no\n" +
                "                ,v.bp_house_number\n" +
                "                ,v.bp_idcard\n" +
                "                ,v.bp_idcard_end_date\n" +
                "                ,v.bp_idcard_start_date\n" +
                "                ,v.bp_image\n" +
                "                ,v.bp_lname\n" +
                "                ,v.bp_name\n" +
                "                ,v.bp_phone\n" +
                "                ,v.bp_pre\n" +
                "                ,v.bp_province\n" +
                "                ,v.bp_sub_district\n" +
                "                ,v.doc_id\n" +
                "                ,d.signature_director_date\n" +
                "                ,d.signature_director_id\n" +
                "                ,d.signature_head_zone_date\n" +
                "                ,d.signature_head_zone_id\n" +
                "      ,d.zone_id " +
                "      ,d.sub_zone " +
                "      ,d.status_appove " +
                "                ,v.doc_id_ref\n" +
                "                ,b.id as id_att_book_bank \n" +
                "                ,b.create_time as create_time_att_book_bank \n" +
                "                ,b.doc_id as doc_id_att_book_bank \n" +
                "                ,b.img_path_front as  img_path_front_att_book_bank \n" +
                "                ,b.img_path_back as  img_path_back_att_book_bank \n" +
                "                ,c.id as id_att_company_seal \n" +
                "                ,c.create_time as create_time_att_company_seal \n" +
                "                ,c.doc_id as doc_id_att_company_seal \n" +
                "                ,c.img_path as img_path_att_company_seal \n" +
                "                ,h.id as id_att_home \n" +
                "                ,h.create_time as create_time_att_home \n" +
                "                ,h.doc_id as doc_id_att_home \n" +
                "                ,h.img_path_front as img_path_front_att_home \n" +
                "                ,h.img_path_back as img_path_back_att_home \n" +
                "                ,i.id as id_att_idcard \n" +
                "                ,i.create_time as create_time_att_idcard \n" +
                "                ,i.doc_id as doc_id_att_idcard \n" +
                "                ,i.img_path_front as img_path_front_att_idcard \n" +
                "                ,i.img_path_back as img_path_back_att_idcard \n" +
                "                ,l.id as id_att_legal_entity \n" +
                "                ,l.create_time as create_time_att_legal_entity \n" +
                "                ,l.doc_id as doc_id_att_legal_entity \n" +
                "                ,l.img_path as img_path_att_legal_entity \n" +
                ",fa.fac_branch \n" +
                ",fa.fac\n" +
                ",fa.fac_district\n" +
                ",fa.fac_head_lname\n" +
                ",fa.fac_head_name\n" +
                ",fa.fac_head_pre\n" +
                ",fa.fac_home\n" +
                ",fa.fac_name\n" +
                ",fa.fac_province\n" +
                ",fa.fac_sub_district\n" +
                ",fa.fac_village_no"+
                ",fa.fac_code"+
                ",fa.fac_que_code"+
                ",si.full_name as name_head_zone"+
                ",si.signature as id_head_zone"+
                ",si1.full_name as name_director"+
                ",si1.signature as id_director"+
                ",si2.full_name as name_manager"+
                ",si2.signature as id_manager"+
                "                from   \n" +
                "                tbl_document d   \n" +
                "                join tbl_document_new_vender v on d.id=v.doc_id_ref  \n" +
                "                left join tbl_att_book_bank b on  b.id=d.att_book_bank_id  \n" +
                "                left join tbl_att_company_seal c on  c.id=d.att_company_seal_id  \n" +
                "                left join tbl_att_home h on  h.id=d.att_home_id  \n" +
                "                left join tbl_att_idcard i on  i.id=d.att_idcard_id  \n" +
                "                left join tbl_att_legal_entity l on  l.id=d.att_legal_entity_id  \n" +
                "left join tbl_factory fa  on  fa.fac=d.doc_fac " +
                "left join tbl_signature si  on  si.user_id=d.signature_head_zone_id  " +
                "left join tbl_signature si1  on  si1.user_id=d.signature_director_id  " +
                "left join tbl_signature si2  on  si2.user_id=d.signature_manager_id  " +
                "                where d.doc_fac = :fac and d.doc_id = :docId and d.doc_year = :year;";
        return (DocumentNewVender) namedParameterJdbcTemplate.queryForObject(SELECTNEWVEBDERBYDOCID, parameters, new NewVenderRowMapper());
    }
    @Transactional
    public DocumentNewVender selectNewVenderByDocId(Long id) {
        Map<String, Long> parameters = new HashMap<>();
        parameters.put("id", id);
        final String SELECTNEWVEBDERBYID = "select top(1) d.id  \n" +
                "                ,d.bp_code  \n" +
                "                ,d.doc_create_date  \n" +
                "                ,d.doc_create_id  \n" +
                "                ,d.doc_create_name  \n" +
                "                ,d.doc_create_position  \n" +
                "                ,d.doc_detail  \n" +
                "                ,d.doc_fac  \n" +
                "                ,d.doc_id  \n" +
                "                ,d.doc_image  \n" +
                "                ,d.doc_status  \n" +
                "                ,d.doc_type_id  \n" +
                "                ,d.doc_year  \n" +
                "                ,d.signature_personal_date  \n" +
                "                ,d.signature_personal_name  \n" +
                "                ,d.signature_personal_pic  \n" +
                "                ,d.signature_personal_pos  \n" +
                "                ,d.signature_farmer_name  \n" +
                "                ,d.signature_farmer_pic  \n" +
                "                ,d.signature_farmer_pos  \n" +
                "                ,d.signature_farmer_date  \n" +
                "      ,d.zone_id " +
                "      ,d.sub_zone " +
                "      ,d.path_pdf " +
                "      ,d.status_pdf " +
                "      ,d.status_appove " +
                "                ,v.bp_birthday\n" +
                "                ,v.bp_code\n" +
                "                ,v.bp_company_date\n" +
                "                ,v.bp_company_idcard\n" +
                "                ,v.bp_company_name\n" +
                "                ,v.bp_company_sta\n" +
                "                ,v.bp_date_create\n" +
                "                ,v.bp_date_update\n" +
                "                ,v.bp_district\n" +
                "                ,v.bp_email\n" +
                "                ,v.bp_full_name\n" +
                "                ,v.bp_house_no\n" +
                "                ,v.bp_house_number\n" +
                "                ,v.bp_idcard\n" +
                "                ,v.bp_idcard_end_date\n" +
                "                ,v.bp_idcard_start_date\n" +
                "                ,v.bp_image\n" +
                "                ,v.bp_lname\n" +
                "                ,v.bp_name\n" +
                "                ,v.bp_phone\n" +
                "                ,v.bp_pre\n" +
                "                ,v.bp_province\n" +
                "                ,v.bp_sub_district\n" +
                "                ,v.doc_id\n" +
                "                ,d.signature_director_date\n" +
                "                ,d.signature_director_id\n" +
                "                ,d.signature_head_zone_date\n" +
                "                ,d.signature_head_zone_id\n" +
                "                ,v.doc_id_ref\n" +
                "                ,b.id as id_att_book_bank \n" +
                "                ,b.create_time as create_time_att_book_bank \n" +
                "                ,b.doc_id as doc_id_att_book_bank \n" +
                "                ,b.img_path_front as  img_path_front_att_book_bank \n" +
                "                ,b.img_path_back as  img_path_back_att_book_bank \n" +
                "                ,c.id as id_att_company_seal \n" +
                "                ,c.create_time as create_time_att_company_seal \n" +
                "                ,c.doc_id as doc_id_att_company_seal \n" +
                "                ,c.img_path as img_path_att_company_seal \n" +
                "                ,h.id as id_att_home \n" +
                "                ,h.create_time as create_time_att_home \n" +
                "                ,h.doc_id as doc_id_att_home \n" +
                "                ,h.img_path_front as img_path_front_att_home \n" +
                "                ,h.img_path_back as img_path_back_att_home \n" +
                "                ,i.id as id_att_idcard \n" +
                "                ,i.create_time as create_time_att_idcard \n" +
                "                ,i.doc_id as doc_id_att_idcard \n" +
                "                ,i.img_path_front as img_path_front_att_idcard \n" +
                "                ,i.img_path_back as img_path_back_att_idcard \n" +
                "                ,l.id as id_att_legal_entity \n" +
                "                ,l.create_time as create_time_att_legal_entity \n" +
                "                ,l.doc_id as doc_id_att_legal_entity \n" +
                "                ,l.img_path as img_path_att_legal_entity \n" +
                ",fa.fac_branch \n" +
                ",fa.fac\n" +
                ",fa.fac_district\n" +
                ",fa.fac_head_lname\n" +
                ",fa.fac_head_name\n" +
                ",fa.fac_head_pre\n" +
                ",fa.fac_home\n" +
                ",fa.fac_name\n" +
                ",fa.fac_province\n" +
                ",fa.fac_sub_district\n" +
                ",fa.fac_village_no"+
                ",fa.fac_code"+
                ",fa.fac_que_code"+
                ",si.full_name as name_head_zone"+
                ",si.signature as id_head_zone"+
                ",si1.full_name as name_director"+
                ",si1.signature as id_director"+
                ",si2.full_name as name_manager"+
                ",si2.signature as id_manager"+
                "                from   \n" +
                "                tbl_document d   \n" +
                "                join tbl_document_new_vender v on d.id=v.doc_id_ref  \n" +
                "                left join tbl_att_book_bank b on  b.id=d.att_book_bank_id  \n" +
                "                left join tbl_att_company_seal c on  c.id=d.att_company_seal_id  \n" +
                "                left join tbl_att_home h on  h.id=d.att_home_id  \n" +
                "                left join tbl_att_idcard i on  i.id=d.att_idcard_id  \n" +
                "                left join tbl_att_legal_entity l on  l.id=d.att_legal_entity_id  \n" +
                "left join tbl_factory fa  on  fa.fac=d.doc_fac " +
                "left join tbl_signature si  on  si.user_id=d.signature_head_zone_id  " +
                "left join tbl_signature si1  on  si1.user_id=d.signature_director_id  " +
                "left join tbl_signature si2  on  si2.user_id=d.signature_manager_id  " +
                "                where d.id = :id;";
        return (DocumentNewVender) namedParameterJdbcTemplate.queryForObject(SELECTNEWVEBDERBYID, parameters, new NewVenderRowMapper());
    }
}
