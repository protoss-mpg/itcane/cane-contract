package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentMapRepository extends JpaRepository<DocumentMap, Long> {
}
