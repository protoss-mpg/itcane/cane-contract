package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentNewFarmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentNewFarmerRepository extends JpaRepository<DocumentNewFarmer, Long> {
}
