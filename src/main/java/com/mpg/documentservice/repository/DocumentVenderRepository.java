package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentNewVender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentVenderRepository extends JpaRepository<DocumentNewVender, Long> {
}
