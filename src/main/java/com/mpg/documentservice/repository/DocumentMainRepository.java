package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentMain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface DocumentMainRepository extends JpaRepository<DocumentMain, Long> {
    @Query(value = "SELECT id, bp_code, doc_create_date, doc_create_id,status_appove, doc_create_name, doc_create_position, doc_detail, doc_fac, doc_id, doc_image, doc_status, doc_type_id, doc_year, signature_farmer_date, signature_farmer_name, signature_farmer_pic, signature_farmer_pos, signature_personal_date, signature_personal_name, signature_personal_pic, signature_personal_pos, att_book_bank_id, att_company_seal_id, att_home_id, att_idcard_id, att_legal_entity_id, path_pdf, status_pdf, signature_head_zone_id, signature_head_zone_date, signature_director_id, signature_director_date, signature_manager_id, signature_manager_date, zone_id, sub_zone\n" +
            "FROM CaneContract.dbo.tbl_document where id IN (:listId) ",
            nativeQuery = true)
    List<Map<String, Object>> findByListId(
            @Param("listId") List<String> listId);

}
