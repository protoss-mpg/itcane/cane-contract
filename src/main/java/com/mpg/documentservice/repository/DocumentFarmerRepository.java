package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.DocumentFarmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentFarmerRepository extends JpaRepository<DocumentFarmer, Long> {
}
