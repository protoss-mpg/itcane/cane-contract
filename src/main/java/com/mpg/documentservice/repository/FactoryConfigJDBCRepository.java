package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.FactoryConfig;
import com.mpg.documentservice.mapper.FactoryRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Repository
public class FactoryConfigJDBCRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional
    public FactoryConfig selectFacByFacCode(String fac) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("fac", fac);
        final String SELECTNEWVEBDERBYFAC = "SELECT id\n" +
                ", fac_branch\n" +
                ", fac\n" +
                ", fac_code\n" +
                ", fac_district\n" +
                ", fac_head_lname\n" +
                ", fac_head_name\n" +
                ", fac_head_pre\n" +
                ", fac_home\n" +
                ", fac_name\n" +
                ", fac_province\n" +
                ", fac_que_code\n" +
                ", fac_sub_district\n" +
                ", fac_village_no\n" +
                "FROM CaneContract.dbo.tbl_factory WHERE fac = :fac ;\n";
        return (FactoryConfig) namedParameterJdbcTemplate.queryForObject(SELECTNEWVEBDERBYFAC, parameters, new FactoryRowMapper());
    }
}
