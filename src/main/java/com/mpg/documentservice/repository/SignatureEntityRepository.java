package com.mpg.documentservice.repository;

import com.mpg.documentservice.entity.SignatureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SignatureEntityRepository extends JpaRepository<SignatureEntity, Long> {
    @Query(value = "SELECT TOP(1) id, fac,user_id,pre_name, name, lname, full_name, email, signature, create_by, create_date, update_by, update_date  FROM tbl_signature WHERE user_id = :userId",
            nativeQuery = true)
    Optional<SignatureEntity> findByUserId(
            @Param("userId") String userId);

}
