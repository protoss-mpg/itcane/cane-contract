package com.mpg.documentservice.util;


import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;


@Component
public class DecodeBase64 {
    public String writeFile(String value, String fac, String year, String fol) {
        String res = null;
        if (value != null) {

            LocalDateTime ldt = LocalDateTime.now();

            String directoryName = "./images/" + fac + "/" + year + "/" + fol + "/" + DateTimeFormatter.ofPattern("yyyy-MM-dd").format(ldt);
            String insertPath = "/images/" + fac + "/" + year + "/" + fol + "/" + DateTimeFormatter.ofPattern("yyyy-MM-dd").format(ldt);
            String password = "" + Math.random();
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            String fileName = sb.toString() + ".jpg";
            File directory = new File(directoryName);
            if (!directory.exists()) {
                directory.mkdir();
            }
            byte[] decodedBytes = Base64.getDecoder().decode(value);
            try {
                FileUtils.writeByteArrayToFile(new File(directoryName + "/" + fileName), decodedBytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            res = insertPath + "/" + fileName;
            return res;

        }
        return res;
    }
}
