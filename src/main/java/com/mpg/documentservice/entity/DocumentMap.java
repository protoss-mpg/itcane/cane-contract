package com.mpg.documentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

@Entity(name = "tbl_document_map")
@PrimaryKeyJoinColumn(name = "doc_id_ref")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DocumentMap extends DocumentMain implements Serializable {
    @Column(name = "doc_id", unique = true)
    private String docIdRef;
    @Column(name = "bp_code")
    private String bpCodeDoc;
    @Column(name = "bp_name")
    private String bpName;
    @Column(name = "cane_ac_id")
    private String caneAcId;
    @Column(name = "bp_idcard")
    private String bpIdCard;

    @Column(name = "f_id")
    private String fId;
    @Column(name = "case_cane_name")
    private String caseCaneName;
    @Column(name = "gis_area")
    private String gisArea;
    @Column(name = "cane_type_name")
    private String caneTypeName;
    @Column(name = "crop_year")
    private String cropYear;
    @Column(name = "type_owner_name")
    private String typeOwnerName;
    @Column(name = "type_owner_full")
    private String typeOwnerFull;
    @Column(name = "check_type")
    private String checkType;
    @Column(name = "input_fid")
    private String inputFid;
    @Column(name = "ban_name")
    private String banName;
    @Column(name = "tam_name")
    private String tamName;
    @Column(name = "amp_name")
    private String ampName;
    @Column(name = "prv_name")
    private String prvName;
    @Column(name = "doc_pdf")
    private String docPdf;
    @Column(name = "doc_desc")
    private String docDesc;
    @Column(name = "path_file_shp")
    private String pathFileShp;
    @Column(name = "path_file_act")
    private String pathFileAct;


    @Transient
    @Column(name = "doc_id_list")
    private List docIdList;

    @Transient
    @Column(name = "data_id_list")
    private List dataIdList;

    @Transient
    @Column(name = "cane_ac_id_list")
    private String caneAcIdList;

    @Transient
    @Column(name = "id_list")
    private String idList;

    @Transient
    @Column(name = "doc_muti_pdf")
    private String docMutiPdf;
}
