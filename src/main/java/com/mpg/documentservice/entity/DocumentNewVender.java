package com.mpg.documentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tbl_document_new_vender")
@PrimaryKeyJoinColumn(name = "doc_id_ref")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DocumentNewVender extends DocumentMain implements Serializable {
    @Column(name = "doc_id", unique = true)
    private String docIdRef;
    @Column(name = "bp_code")
    private String bpCodeDoc;
    @Column(name = "bp_idcard")
    private String bpIdCard;
    @Column(name = "bp_pre")
    private String bpPre;
    @Column(name = "bp_name")
    private String bpName;
    @Column(name = "bp_lname")
    private String bpLname;
    @Column(name = "bp_full_name")
    private String bpFullName;
    @Column(name = "bp_image")
    private String bpImage;
    @Column(name = "bp_house_number")
    private String bpHouseNumber;
    @Column(name = "bp_house_no")
    private String bpHouseNo;
    @Column(name = "bp_sub_district")
    private String bpSubDistrict;
    @Column(name = "bp_district")
    private String bpDistrict;
    @Column(name = "bp_province")
    private String bpProvince;
    @Column(name = "bp_birthday")
    private String bpBirthday;
    @Column(name = "bp_idcard_start_date")
    private String bpIdCardStartDate;
    @Column(name = "bp_idcard_end_date")
    private String bpIdCardEndDate;
    @Column(name = "bp_company_idcard")
    private String bpCompanyIdCard;
    @Column(name = "bp_company_name")
    private String bpCompanyName;
    @Column(name = "bp_company_date")
    private String bpCompanyDate;
    @Column(name = "bp_company_sta")
    private String bpCompanySta;
    @Column(name = "bp_phone")
    private String bpPhone;
    @Column(name = "bp_email")
    private String bpEmail;
    @Column(name = "bp_date_create")
    private Date bpDateCreate;
    @Column(name = "bp_date_update")
    private Date bpDateUpdate;



}
