package com.mpg.documentservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tbl_att_idcard")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class AttachIdCard implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "img_path_front")
    private String imgPathFront;
    @Column(name = "img_path_back")
    private String imgPathBack;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "doc_id", unique = true)
    private String docId;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "attachIdCard")
    @JsonIgnore
    private DocumentMain documentMain;
}
