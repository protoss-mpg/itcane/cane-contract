package com.mpg.documentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tbl_document")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@NoArgsConstructor
@ToString
public abstract class DocumentMain implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "doc_id", unique = true)
    private String docId;
    @Column(name = "doc_create_id")
    private String docCreateId;
    @Column(name = "doc_create_date")
    private Date docCreateDate;
    @Column(name = "doc_create_name")
    private String docCreateName;
    @Column(name = "doc_create_position")
    private String docCreatePosition;
    @Column(name = "doc_detail")
    private String docDetail;
    @Column(name = "doc_type_id")
    private String docTypeId;
    @Column(name = "doc_fac")
    private String docFac;
    @Column(name = "doc_year")
    private String docYear;
    @Column(name = "doc_image")
    private String docImage;
    @Column(name = "doc_status")
    private String docStatus;
    @Column(name = "bp_code")
    private String bpCode;
    @Column(name = "signature_personal_name")
    private String signaturePersonalName;
    @Column(name = "signature_personal_pic",columnDefinition = "varchar(MAX)")
    private String signaturePersonalPic;
    @Column(name = "signature_personal_pos")
    private String signaturePersonalPos;
    @Column(name = "signature_personal_date")
    private Date signaturePersonalDate;
    @Column(name = "signature_farmer_name")
    private String signatureFarmerName;
    @Column(name = "signature_farmer_pic",columnDefinition = "varchar(MAX)")
    private String signatureFarmerPic;
    @Column(name = "signature_farmer_pos")
    private String signatureFarmerPos;
    @Column(name = "signature_farmer_date")
    private Date signatureFarmerDate;
    @Column(name = "path_pdf")
    private String pathPdf;
    @Column(name = "status_pdf")
    private String statusPdf;
    @Column(name = "zone_id")
    private String zoneId;
    @Column(name = "sub_zone")
    private String subZone;

    @Column(name = "status_appove")
    private String statusAppove;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "att_idcard_id", referencedColumnName = "id")
    private AttachIdCard attachIdCard;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "att_home_id", referencedColumnName = "id")
    private AttachHome attachHome;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "att_book_bank_id", referencedColumnName = "id")
    private AttachBookBank attachBookBank;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "att_legal_entity_id", referencedColumnName = "id")
    private AttachLegalEntity attachLegalEntity;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "att_company_seal_id", referencedColumnName = "id")
    private AttachCompanySeal attachCompanySeal;

    @Transient
    @Column(name = "fac_name")
    private String facName;
    @Transient
    @Column(name = "fac_home")
    private String facHome;
    @Transient
    @Column(name = "fac_village_no")
    private String facVillageNo;
    @Transient
    @Column(name = "fac_sub_district")
    private String facSubDistrict;
    @Transient
    @Column(name = "fac_district")
    private String facDistrict;
    @Transient
    @Column(name = "fac_province")
    private String facProvince;
    @Transient
    @Column(name = "fac_branch")
    private String branch;

    @Transient
    @Column(name = "fac_head_pre")
    private String facHeadPre;
    @Transient
    @Column(name = "fac_head_name")
    private String facHeadName;
    @Transient
    @Column(name = "fac_head_lname")
    private String facHeadLname;

    @Transient
    @Column(name = "bd_count_year")
    private String bdCountYear;
    @Transient
    @Column(name = "fac_code")
    private String facCode;
    @Transient
    @Column(name = "fac_que_code")
    private String facQueCode;


    @Column(name = "signature_head_zone_id")
    private String signatureHeadzoneId;

    @Transient
    @Column(name = "signature_head_zone_full_name")
    private String signatureHeadzoneFullName;

    @Transient
    @Column(name = "signature_head_zone_pic",columnDefinition = "varchar(MAX)")
    private String signatureHeadzonePic;

    @Column(name = "signature_head_zone_date")
    private Date signatureHeadzoneDate;

    @Column(name = "signature_director_id")
    private String signatureDirectorId;

    @Transient
    @Column(name = "signature_director_full_name")
    private String signatureDirectorFullName;

    @Transient
    @Column(name = "signature_director_pic",columnDefinition = "varchar(MAX)")
    private String signatureDirectorPic;


    @Column(name = "signature_director_date")
    private Date signatureDirectorDate;



    @Column(name = "signature_manager_id")
    private String signatureManagerId;

    @Transient
    @Column(name = "signature_manager_full_name")
    private String signatureManagerFullName;

    @Transient
    @Column(name = "signature_manager_pic",columnDefinition = "varchar(MAX)")
    private String signatureManagerPic;

    @Column(name = "signature_manager_date")
    private Date signatureManagerDate;

    @Transient
    @Column(name = "process_id")
    private String processId;

    @Transient
    @Column(name = "task_id")
    private String taskId;

    @Column(name = "flow_process_id")
    private Long flowProcessId;
}
