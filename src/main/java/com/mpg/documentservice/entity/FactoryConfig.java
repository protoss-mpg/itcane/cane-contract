package com.mpg.documentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "tbl_factory")
@Table(indexes = {@Index(columnList = "fac")})
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class FactoryConfig implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "fac", unique = true)
    private String fac;
    @Column(name = "fac_name")
    private String facName;
    @Column(name = "fac_home")
    private String facHome;
    @Column(name = "fac_village_no")
    private String facVillageNo;
    @Column(name = "fac_sub_district")
    private String facSubDistrict;
    @Column(name = "fac_district")
    private String facDistrict;
    @Column(name = "fac_province")
    private String facProvince;
    @Column(name = "fac_branch")
    private String branch;

    @Column(name = "fac_code")
    private String facCode;
    @Column(name = "fac_que_code")
    private String facQueCode;

    @Column(name = "fac_head_pre")
    private String facHeadPre;
    @Column(name = "fac_head_name")
    private String facHeadName;
    @Column(name = "fac_head_lname")
    private String facHeadLname;
}
