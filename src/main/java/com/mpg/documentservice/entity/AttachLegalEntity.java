package com.mpg.documentservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tbl_att_legal_entity")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class AttachLegalEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "img_path")
    private String imgPath;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "doc_id", unique = true)
    private String docId;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "attachLegalEntity")
    @JsonIgnore
    private DocumentMain documentMain;
}
