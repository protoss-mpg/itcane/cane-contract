package com.mpg.documentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "tbl_signature")
@Table(indexes = {@Index(columnList = "fac"),@Index(columnList = "user_id")})
@Getter
@Setter
@NoArgsConstructor
@ToString
public class SignatureEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "fac")
    private String fac;

    @Column(name = "user_id",nullable = false,unique = true)
    private String userId;
    @Column(name = "pre_name")
    private String preName;
    @Column(name = "name")
    private String name;
    @Column(name = "lname")
    private String lName;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "email")
    private String email;
    @Column(name = "signature",nullable = false)
    private String signature;

    @Column(name = "create_by")
    private String createBy;
    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "update_by")
    private String updateBy;
    @Column(name = "update_date")
    private Date updateDate;
}
