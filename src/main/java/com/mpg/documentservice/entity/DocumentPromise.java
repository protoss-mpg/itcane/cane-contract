package com.mpg.documentservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;
import java.io.Serializable;

@Entity(name = "tbl_document_promise")
@PrimaryKeyJoinColumn(name = "doc_id_ref")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DocumentPromise extends DocumentMain implements Serializable {
    @Column(name = "doc_id", unique = true)
    private String docIdRef;
    @Column(name = "bp_code")
    private String bpCodeDoc;

    @Column(name = "bp_name")
    private String bpName;
    @Column(name = "bp_pre_name")
    private String bpPreName;
    @Column(name = "bp_f_name")
    private String bpFName;
    @Column(name = "bp_l_name")
    private String bpLName;


    @Transient
    private Object resArray;

    @Column(name = "doc_ton")
    private String ton;
    @Column(name = "doc_desc")
    private String docDesc;
}
