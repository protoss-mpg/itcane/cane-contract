package com.mpg.documentservice.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mpg.documentservice.entity.DocumentMain;
import com.mpg.documentservice.repository.DocumentMainRepository;
import com.mpg.documentservice.util.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FlowService {

    private RestTemplate restTemplate;
    @Value("${url.flowservice}")
    private String urlFlow;
    @Autowired
    private DocumentMainRepository documentMainRepository;

    public Boolean createFlow(Long id, String docStatus, List appoveLine, String createBy, String email) {
        log.info("initService=FlowService.createFlow");
        restTemplate = new RestTemplate();
        String JsonRes = "";
        JsonObject finalRes;
        Map<String,Object> res = new HashMap();
        Gson g = new Gson();
        final String flowPath = urlFlow + "createProcess/ITCANE/" + docStatus;

        res.put("documentNumber", id);
        res.put("description", "");
        res.put("creater", createBy);
        res.put("createrEmail", email);
        res.put("requester", createBy);
        res.put("requesterEmail", email);
        res.put("details", appoveLine);
        String requestJsonBody = JSONUtil.getJSONSerializer().include("details").serialize(res);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity(requestJsonBody, headers);
            URI uri = new URI(flowPath);
            JsonRes = restTemplate.postForObject(uri, request, String.class);

        } catch (URISyntaxException e) {
            log.error("ErrorUri={}", e.getMessage());
        }
        finalRes = g.fromJson(JsonRes, JsonObject.class);
        DocumentMain updateData= documentMainRepository.getOne(id);
        updateData.setFlowProcessId(finalRes.get("data").getAsJsonObject().get("processData").getAsJsonObject().get("id").getAsLong());
        documentMainRepository.save(updateData);
        log.info("successService=FlowService.createFlow");
        return Boolean.parseBoolean(String.valueOf(finalRes.get("success")));

    }

    public Boolean terminateFlow(String systemCode, String processId) {
        log.info("initService=FlowService.terminateFlow");
        String JsonRes = "";
        Gson g = new Gson();
        JsonObject finalRes;
        restTemplate = new RestTemplate();
        final String terminateFlowPath = urlFlow + "terminateProcess/"+systemCode+"/" + processId;
        URI uri = null;
        try {
            uri = new URI(terminateFlowPath);
        } catch (URISyntaxException e) {
            log.error("ErrorUri={}", e.getMessage());
        }
        JsonRes = restTemplate.getForObject(uri, String.class);
        finalRes = g.fromJson(JsonRes, JsonObject.class);
        log.info("successService=FlowService.terminateFlow");
        return Boolean.parseBoolean(String.valueOf(finalRes.get("success")));
    }
}
