package com.mpg.documentservice.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mpg.documentservice.entity.*;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.model.MapMutiResponse;
import com.mpg.documentservice.repository.DocumentMapJDBCRepository;
import com.mpg.documentservice.repository.DocumentMapRepository;
import com.mpg.documentservice.util.DecodeBase64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class DocumentMapService {
    @Value("${pattern.time}")
    private String pattern;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${folder.idcard}")
    private String idCard;
    @Value("${folder.bookbank}")
    private String bookBank;
    @Value("${folder.companyseal}")
    private String companySeal;
    @Value("${folder.legalentity}")
    private String legalEntity;
    @Value("${folder.homebook}")
    private String homeBook;
    @Value("${folder.docimg}")
    private String docImg;
    @Value("${label.DELETESUCCESS}")
    private String deleteSS;

    @Value("${url.gisservice}")
    private String gisService;

    @Autowired
    private DocumentMapRepository documentMapRepository;
    @Autowired
    private DocumentMapJDBCRepository documentMapJDBCRepository;
    @Autowired
    private DecodeBase64 decodeBase64;

    @Autowired
    private DocumentMainService documentMainService;

    private RestTemplate restTemplate;

    private static final String DATABEFOREUPDATE = "DataBeforeUpdate={}";
    private static final String DATEAFTERUPDATE = "DataAfterUpdate={}";

    @Autowired
    private FlowService flowService;

    @Value("${url.adminservice}")
    private String urlAdmin;

    @Transactional
    public MapMutiResponse insertMapMuti(DocumentMap data) {
        log.info("initService=DocumentMapService.insertMapMuti");
        MapMutiResponse mapMutiResponse = new MapMutiResponse();

        String[] splitted = data.getCaneAcIdList().split(",");
        String arUrlString = "?";

        for (int i = 0; i < splitted.length; i++) {
            if (arUrlString.equals("?")) {
                arUrlString += "caneacids=" + splitted[i];
            } else {
                arUrlString += "&caneacids=" + splitted[i];
            }
        }
        restTemplate = new RestTemplate();
        String mapDetailPath = gisService + "mapdetail/api/v1/" + data.getDocFac() + "/" + data.getDocYear() + arUrlString;
        String mapAddressPath = gisService + "mapaddress/api/v1/" + data.getDocFac() + "/" + data.getDocYear() + arUrlString;
        URI uriMapDetailPath = null;
        URI uriMapAddressPath = null;

        List dataId = new ArrayList();
        List docId = new ArrayList();
        try {
            uriMapDetailPath = new URI(mapDetailPath);
            uriMapAddressPath = new URI(mapAddressPath);
        } catch (URISyntaxException e) {
            log.error("UriError={}", e.getMessage());
        }
        Gson g = new Gson();
        List mapDetailList = g.fromJson(restTemplate.getForObject(uriMapDetailPath, String.class), List.class);
        List mapAddressList = g.fromJson(restTemplate.getForObject(uriMapAddressPath, String.class), List.class);


        if (splitted.length == mapDetailList.size()) {

            for (int i = 0; i < mapDetailList.size(); i++) {
                DocumentMap documentMap = new DocumentMap();

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                documentMap.setDocFac(data.getDocFac());
                documentMap.setDocCreateId(data.getDocCreateId());
                documentMap.setDocStatus(data.getDocStatus());
                documentMap.setZoneId(data.getZoneId());
                documentMap.setSubZone(data.getSubZone());
                documentMap.setDocId("MPFM" + simpleDateFormat.format(new Date()));
                documentMap.setDocCreateDate(new Date());
                documentMap.setDocIdRef(documentMap.getDocId());
                documentMap.setSignaturePersonalPic(data.getSignaturePersonalPic());
                documentMap.setSignaturePersonalName(data.getSignaturePersonalName());
                documentMap.setSignaturePersonalDate(new Date());
                documentMap.setSignatureFarmerPic(data.getSignatureFarmerPic());
                documentMap.setSignatureFarmerName(data.getSignatureFarmerName());
                documentMap.setSignatureFarmerDate(new Date());
                String jsonMapDetailList = g.toJson(mapDetailList.get(i));
                JsonObject oFinalDataMapDetailList = g.fromJson(jsonMapDetailList, JsonObject.class);

                documentMap.setBpCodeDoc(oFinalDataMapDetailList.get("qoataId").getAsString());
                documentMap.setBpCode(oFinalDataMapDetailList.get("qoataId").getAsString());

                documentMap.setPathFileShp(oFinalDataMapDetailList.get("imageFID").getAsString());
                documentMap.setPathFileAct(oFinalDataMapDetailList.get("imageActivity").getAsString());

                documentMap.setBpName(oFinalDataMapDetailList.get("frmName").getAsString());
                documentMap.setCaneAcId(oFinalDataMapDetailList.get("caneAcId").getAsString());
                documentMap.setBpIdCard(oFinalDataMapDetailList.get("centerId").getAsString());
                documentMap.setZoneId(oFinalDataMapDetailList.get("zoneId").getAsString());
                documentMap.setSubZone(oFinalDataMapDetailList.get("subZone").getAsString());
                documentMap.setFId(oFinalDataMapDetailList.get("fid").getAsString());
                documentMap.setCaseCaneName(oFinalDataMapDetailList.get("caseCaneName").getAsString());
                documentMap.setGisArea(oFinalDataMapDetailList.get("gisArea").getAsString());
                documentMap.setCaneTypeName(oFinalDataMapDetailList.get("caneTypeName").getAsString());
                documentMap.setCropYear(oFinalDataMapDetailList.get("cropYear").getAsString());
                documentMap.setTypeOwnerName(oFinalDataMapDetailList.get("typeOwnerName").getAsString());
                documentMap.setTypeOwnerFull(oFinalDataMapDetailList.get("farmerPre").getAsString() + " " + oFinalDataMapDetailList.get("farmerName").getAsString() + "  " + oFinalDataMapDetailList.get("farmerLname").getAsString());
                documentMap.setCheckType(oFinalDataMapDetailList.get("checkType").getAsString());
                documentMap.setDocYear(oFinalDataMapDetailList.get("cropYear").getAsString());
                if (mapAddressList.size() != 0) {

                    String jsonMapAddressList = g.toJson(mapAddressList.get(i));
                    JsonObject oFinalDataMapAddressList = g.fromJson(jsonMapAddressList, JsonObject.class);
                    documentMap.setInputFid(oFinalDataMapAddressList.get("id").getAsString());
                    documentMap.setBanName(oFinalDataMapAddressList.get("banName").getAsString());
                    documentMap.setTamName(oFinalDataMapAddressList.get("tumName").getAsString());
                    documentMap.setAmpName(oFinalDataMapAddressList.get("ampName").getAsString());
                    documentMap.setPrvName(oFinalDataMapAddressList.get("prvName").getAsString());

                }

                documentMapRepository.saveAndFlush(documentMap);

                documentMainService.updateSignApproveAll(documentMap);

                String adminPath = urlAdmin + "roleapprovegis/api/v1/" + documentMap.getDocFac() + "/" + documentMap.getDocCreateId();
                URI uriAdminPath = null;
                try {
                    uriAdminPath = new URI(adminPath);

                } catch (URISyntaxException e) {
                    log.error("ErrorUri={}", e.getMessage());
                }
                List<Map<String, Object>> appove = new ArrayList();

                JsonArray jsonArray = g.fromJson(restTemplate.getForObject(uriAdminPath, String.class), JsonArray.class);
                JsonObject yourJson = jsonArray.get(0).getAsJsonObject();
                Map<String, Object> objectMap1 = new HashMap();
                objectMap1.put("sequence", 1);
                objectMap1.put("usernameApprover", yourJson.get("headzoneId").getAsString());
                objectMap1.put("roleFlow", "VRF");
                objectMap1.put("userId", yourJson.get("headzoneId").getAsString());
                objectMap1.put("email", yourJson.get("headzoneEmail").getAsString());
                appove.add(objectMap1);

//                Map<String, Object> objectMap2 = new HashMap();
//                objectMap2.put("sequence", 2);
//                objectMap2.put("usernameApprover", yourJson.get("managerFullName").getAsString());
//                objectMap2.put("roleFlow", "VRF");
//                objectMap2.put("userId", yourJson.get("managerId").getAsString());
//                objectMap2.put("email", yourJson.get("managerEmail").getAsString());
//                appove.add(objectMap2);

                Map<String, Object> objectMap3 = new HashMap();
                objectMap3.put("sequence", 2);
                objectMap3.put("usernameApprover", yourJson.get("directorId").getAsString());
                objectMap3.put("roleFlow", "APR");
                objectMap3.put("userId", yourJson.get("directorId").getAsString());
                objectMap3.put("email", yourJson.get("directorEmail").getAsString());
                appove.add(objectMap3);
                Boolean chkFlow = flowService.createFlow(documentMap.getId(), documentMap.getDocStatus(), appove, yourJson.get("subzoneEmail").getAsString().split("@")[0], yourJson.get("subzoneEmail").getAsString());
                if (chkFlow == false) {
                    try {
                        throw new Exception(
                                "FlowService Return False"
                        );
                    } catch (Exception e) {
                        log.error("ErrorFlowService={}", e.getMessage());
                    }
                }


                dataId.add(documentMap.getId());
                docId.add(documentMap.getDocId());
            }


            mapMutiResponse.setStatusMsg(labelSuccess);
            mapMutiResponse.setCodeTypeMsg(labelComplete);
            mapMutiResponse.setCaneAcId(data.getCaneAcIdList());
            mapMutiResponse.setDocId(docId);
            mapMutiResponse.setDataId(dataId);
        } else {
            mapMutiResponse.setStatusMsg(labelError);
            mapMutiResponse.setCodeTypeMsg(labelError);
            mapMutiResponse.setMsg("Data Insert Not equal Data from Service");
        }

        log.info("successService=DocumentMapService.insertMapMuti");
        return mapMutiResponse;
    }

    @Transactional
    public DocumentResponse insertMap(DocumentMap data) {
        log.info("initService=DocumentMapService.insertMap");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() == null) {
            String msgWarning = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            data.setDocId("MPFM" + simpleDateFormat.format(new Date()));

            data.setDocCreateDate(new Date());


            data.setDocIdRef(data.getDocId());
            data.setBpCodeDoc(data.getBpCode());
            try {
                String pathImg = decodeBase64.writeFile(data.getDocImage(), data.getDocFac(), data.getDocYear(), docImg);
                data.setDocImage(pathImg);
            } catch (NullPointerException e) {
                log.info("docImg is null");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), data.getDocFac(), data.getDocYear(), idCard);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), data.getDocFac(), data.getDocYear(), idCard);
                AttachIdCard attachIdCard = new AttachIdCard();
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(data.getDocId());
                data.setAttachIdCard(attachIdCard);
            } catch (NullPointerException e) {
                msgWarning += "[attachIdCard is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachIdCard is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachCompanySeal().getImgPath(), data.getDocFac(), data.getDocYear(), companySeal);
                AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
                attachCompanySeal.setImgPath(getImgPathPath);
                attachCompanySeal.setCreateTime(new Date());
                attachCompanySeal.setDocId(data.getDocId());
                data.setAttachCompanySeal(attachCompanySeal);
            } catch (NullPointerException e) {
                msgWarning += "[attachCompanySeal is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachCompanySeal is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachLegalEntity().getImgPath(), data.getDocFac(), data.getDocYear(), legalEntity);
                AttachLegalEntity attachLegalEntity = new AttachLegalEntity();
                attachLegalEntity.setImgPath(getImgPathPath);
                attachLegalEntity.setCreateTime(new Date());
                attachLegalEntity.setDocId(data.getDocId());
                data.setAttachLegalEntity(attachLegalEntity);
            } catch (NullPointerException e) {
                msgWarning += "[attachLegalEntity is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachLegalEntity is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), data.getDocFac(), data.getDocYear(), bookBank);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), data.getDocFac(), data.getDocYear(), bookBank);
                AttachBookBank attachBookBank = new AttachBookBank();
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(data.getDocId());
                data.setAttachBookBank(attachBookBank);
            } catch (NullPointerException e) {
                msgWarning += "[attachBookBank is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachBookBank is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), data.getDocFac(), data.getDocYear(), homeBook);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), data.getDocFac(), data.getDocYear(), homeBook);
                AttachHome attachHome = data.getAttachHome();
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(data.getDocId());
                data.setAttachHome(attachHome);
            } catch (NullPointerException e) {
                msgWarning += "[attachHome is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachHome is null]");
            }
            documentMapRepository.save(data);
            if (msgWarning.length() != 0) {
                documentResponse.setMsg(msgWarning);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelWarning);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg("Insert Success");
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            }
        } else {
            documentResponse.setMsg("Can't Insert Id");
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMapService.insertMap");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse deleteMap(String data) {
        log.info("initService=DocumentMapService.deleteMap");
        DocumentResponse documentResponse = new DocumentResponse();
        try {
            flowService.terminateFlow("ITCANE", documentMapRepository.getOne(Long.valueOf(data)).getFlowProcessId().toString());
        } catch (NullPointerException e) {
            log.error("ErrorNull={}", e.getMessage());
        }
        documentMapRepository.deleteById(Long.valueOf(data));
        documentResponse.setMsg(deleteSS);
        documentResponse.setStatusMsg(labelSuccess);
        documentResponse.setCodeTypeMsg(labelComplete);
        documentResponse.setDataId(Long.valueOf(data));


        log.info("successService=DocumentMapService.deleteMap");
        return documentResponse;
    }


    @Transactional
    public MapMutiResponse deleteMutiMap(DocumentMap data) {
        log.info("initService=DocumentMapService.deleteMutiMap");
        MapMutiResponse mapMutiResponse = new MapMutiResponse();
        List dataId = new ArrayList();
        String[] splitIdList = data.getIdList().split(",");
        for (int i = 0; i < splitIdList.length; i++) {
            try {
                flowService.terminateFlow("ITCANE", documentMapRepository.getOne(Long.valueOf(splitIdList[i])).getFlowProcessId().toString());
            } catch (NullPointerException e) {
                log.error("ErrorNull={}", e.getMessage());
            }
            documentMapRepository.deleteById(Long.parseLong(splitIdList[i]));
            dataId.add(splitIdList[i]);
        }
        mapMutiResponse.setMsg(deleteSS);
        mapMutiResponse.setStatusMsg(labelSuccess);
        mapMutiResponse.setCodeTypeMsg(labelComplete);
        mapMutiResponse.setDataId(dataId);
        log.info("successService=DocumentMapService.deleteMutiMap");
        return mapMutiResponse;
    }


    @Transactional
    public DocumentMap selectMapByDocId(String docId, String fac, String year) {
        log.info("initService=DocumentMapService.selectMapByDocId");
        log.info("successService=DocumentMapService.selectMapByDocId");
        return documentMapJDBCRepository.selectMapByDocId(docId, fac, year);
    }

    @Transactional
    public DocumentMap selectMapById(Long id) {
        log.info("initService=DocumentMapService.selectMapById");
        log.info("successService=DocumentMapService.selectMapById");
        return documentMapJDBCRepository.selectMapById(id);
    }


    @Transactional
    public MapMutiResponse updateMutiPDF(DocumentMap data) {
        log.info("initService=DocumentMapService.updateMutiPDF");
        MapMutiResponse mapMutiResponse = new MapMutiResponse();
        List dataId = new ArrayList();
        List docId = new ArrayList();

        String[] splitIdList = data.getIdList().split(",");
        String[] splitMutiPdf = data.getDocMutiPdf().split(",");

        if (splitIdList.length == splitMutiPdf.length) {

            for (int i = 0; i < splitIdList.length; i++) {

                DocumentMap documentNewFarmerOptional = documentMapRepository.getOne(Long.parseLong(splitIdList[i]));
                log.info(DATABEFOREUPDATE, documentNewFarmerOptional.toString());

                documentNewFarmerOptional.setStatusPdf("Y");
                documentNewFarmerOptional.setPathPdf(splitMutiPdf[i]);
                documentMapRepository.save(documentNewFarmerOptional);
                log.info(DATEAFTERUPDATE, documentNewFarmerOptional.toString());
                dataId.add(documentNewFarmerOptional.getId());
                docId.add(documentNewFarmerOptional.getDocId());

            }
            mapMutiResponse.setMsg(labelUpSS);
            mapMutiResponse.setStatusMsg(labelSuccess);
            mapMutiResponse.setCodeTypeMsg(labelComplete);
            mapMutiResponse.setDocId(docId);
            mapMutiResponse.setDataId(dataId);
        } else {
            mapMutiResponse.setStatusMsg(labelError);
            mapMutiResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMapService.updateMutiPDF");
        return mapMutiResponse;
    }
}
