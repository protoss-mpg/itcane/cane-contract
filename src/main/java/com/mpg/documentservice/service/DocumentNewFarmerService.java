package com.mpg.documentservice.service;

import com.mpg.documentservice.entity.*;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.repository.DocumentNewFarmerJDBCRepository;
import com.mpg.documentservice.repository.DocumentNewFarmerRepository;
import com.mpg.documentservice.util.DecodeBase64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;


@Service
@Slf4j
public class DocumentNewFarmerService {
    @Value("${pattern.time}")
    private String pattern;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${folder.idcard}")
    private String idCard;
    @Value("${folder.bookbank}")
    private String bookBank;
    @Value("${folder.companyseal}")
    private String companySeal;
    @Value("${folder.legalentity}")
    private String legalEntity;
    @Value("${folder.homebook}")
    private String homeBook;
    @Value("${folder.docimg}")
    private String docImg;
    @Value("${label.DELETESUCCESS}")
    private String deleteSS;

    @Autowired
    private FlowService flowService;

    @Autowired
    private DecodeBase64 decodeBase64;

    @Autowired
    private DocumentNewFarmerRepository documentNewFarmerRepository;

    @Autowired
    private DocumentNewFarmerJDBCRepository documentNewFarmerJDBCRepository;

    private static final String DATABEFOREUPDATE = "DataBeforeUpdate={}";
    private static final String DATEAFTERUPDATE = "DataAfterUpdate={}";

    @Transactional
    public DocumentResponse insertNewFarmer(DocumentNewFarmer data) {
        log.info("initService=DocumentNewFarmerService.insertNewFarmer");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() == null) {
            String msgWarning = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            data.setDocCreateDate(new Date());

            try {
                String pathImg = decodeBase64.writeFile(data.getDocImage(), data.getDocFac(), data.getDocYear(), docImg);
                data.setDocImage(pathImg);
            } catch (NullPointerException e) {
                log.info("docImg is null");
            }
            data.setDocId("MPFM" + simpleDateFormat.format(new Date()));

            data.setBpDateCreate(new Date());

            data.setDocIdRef(data.getDocId());
            data.setBpCodeDoc(data.getBpCode());
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), data.getDocFac(), data.getDocYear(), idCard);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), data.getDocFac(), data.getDocYear(), idCard);
                AttachIdCard attachIdCard = new AttachIdCard();
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(data.getDocId());
                data.setAttachIdCard(attachIdCard);
            } catch (NullPointerException e) {
                msgWarning += "[attachIdCard is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachIdCard is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachCompanySeal().getImgPath(), data.getDocFac(), data.getDocYear(), companySeal);
                AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
                attachCompanySeal.setImgPath(getImgPathPath);
                attachCompanySeal.setCreateTime(new Date());
                attachCompanySeal.setDocId(data.getDocId());
                data.setAttachCompanySeal(attachCompanySeal);
            } catch (NullPointerException e) {
                msgWarning += "[attachCompanySeal is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachCompanySeal is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachLegalEntity().getImgPath(), data.getDocFac(), data.getDocYear(), legalEntity);
                AttachLegalEntity attachLegalEntity = new AttachLegalEntity();
                attachLegalEntity.setImgPath(getImgPathPath);
                attachLegalEntity.setCreateTime(new Date());
                attachLegalEntity.setDocId(data.getDocId());
                data.setAttachLegalEntity(attachLegalEntity);
            } catch (NullPointerException e) {
                msgWarning += "[attachLegalEntity is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachLegalEntity is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), data.getDocFac(), data.getDocYear(), bookBank);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), data.getDocFac(), data.getDocYear(), bookBank);
                AttachBookBank attachBookBank = new AttachBookBank();
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(data.getDocId());
                data.setAttachBookBank(attachBookBank);
            } catch (NullPointerException e) {
                msgWarning += "[attachBookBank is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachBookBank is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), data.getDocFac(), data.getDocYear(), homeBook);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), data.getDocFac(), data.getDocYear(), homeBook);
                AttachHome attachHome = data.getAttachHome();
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(data.getDocId());
                data.setAttachHome(attachHome);
            } catch (NullPointerException e) {
                msgWarning += "[attachHome is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachHome is null]");
            }
            documentNewFarmerRepository.save(data);
            if (msgWarning.length() != 0) {
                documentResponse.setMsg(msgWarning);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelWarning);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg(labelInsSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            }
        } else {
            documentResponse.setMsg("Can't Insert Id");
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentNewFarmerService.insertNewFarmer");


        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateAttachHomeIdCardBookBank(DocumentNewFarmer data) {
        log.info("initService=DocumentNewFarmerService.updateAttachHomeIdCardBookBank");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {
            String getImgPathBackPath;
            String getImgPathFrontPath;
            AttachHome attachHome = new AttachHome();
            AttachBookBank attachBookBank = new AttachBookBank();
            AttachIdCard attachIdCard = new AttachIdCard();

            DocumentNewFarmer documentNewFarmerRepositoryOne = documentNewFarmerRepository.getOne(data.getId());
            log.info(DATABEFOREUPDATE, documentNewFarmerRepositoryOne.toString());

            try {
                log.info("modeUpdate Id={}", documentNewFarmerRepositoryOne.getAttachHome().getId());
                if (documentNewFarmerRepositoryOne.getAttachHome().getId() != null && data.getAttachHome().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), homeBook);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), homeBook);
                    attachHome = documentNewFarmerRepositoryOne.getAttachHome();
                    attachHome.setImgPathFront(getImgPathFrontPath);
                    attachHome.setImgPathBack(getImgPathBackPath);
                    attachHome.setCreateTime(new Date());
                    documentNewFarmerRepositoryOne.setAttachHome(attachHome);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), homeBook);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), homeBook);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(documentNewFarmerRepositoryOne.getDocId());
                documentNewFarmerRepositoryOne.setAttachHome(attachHome);
            }


            try {
                log.info("modeUpdate Id={}", documentNewFarmerRepositoryOne.getAttachIdCard().getId());

                if (documentNewFarmerRepositoryOne.getAttachIdCard().getId() != null && data.getAttachIdCard().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), idCard);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), idCard);
                    attachIdCard = documentNewFarmerRepositoryOne.getAttachIdCard();
                    attachIdCard.setImgPathFront(getImgPathFrontPath);
                    attachIdCard.setImgPathBack(getImgPathBackPath);
                    attachIdCard.setCreateTime(new Date());
                    documentNewFarmerRepositoryOne.setAttachIdCard(attachIdCard);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), idCard);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), idCard);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(documentNewFarmerRepositoryOne.getDocId());
                documentNewFarmerRepositoryOne.setAttachIdCard(attachIdCard);
            }

            try {
                log.info("modeUpdate Id={}", documentNewFarmerRepositoryOne.getAttachBookBank().getId());

                if (documentNewFarmerRepositoryOne.getAttachBookBank().getId() != null && data.getAttachBookBank().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), bookBank);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), bookBank);
                    attachBookBank = documentNewFarmerRepositoryOne.getAttachBookBank();
                    attachBookBank.setImgPathFront(getImgPathFrontPath);
                    attachBookBank.setImgPathBack(getImgPathBackPath);
                    attachBookBank.setCreateTime(new Date());
                    documentNewFarmerRepositoryOne.setAttachBookBank(attachBookBank);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), bookBank);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), documentNewFarmerRepositoryOne.getDocFac(), documentNewFarmerRepositoryOne.getDocYear(), bookBank);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(documentNewFarmerRepositoryOne.getDocId());
                documentNewFarmerRepositoryOne.setAttachBookBank(attachBookBank);
            }
            documentNewFarmerRepository.save(documentNewFarmerRepositoryOne);
            log.info(DATEAFTERUPDATE, documentNewFarmerRepositoryOne.toString());
            documentResponse.setMsg(labelUpSS);
            documentResponse.setStatusMsg(labelSuccess);
            documentResponse.setCodeTypeMsg(labelComplete);
            documentResponse.setDocId(data.getDocId());
            documentResponse.setDataId(data.getId());


        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentNewFarmerService.updateAttachHomeIdCardBookBank");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse deleteNewFarmer(String data) {
        log.info("initService=DocumentNewFarmerService.deleteNewFarmer");
        DocumentResponse documentResponse = new DocumentResponse();
        try {
            flowService.terminateFlow("ITCANE", documentNewFarmerRepository.getOne(Long.valueOf(data)).getFlowProcessId().toString());
        } catch (NullPointerException e) {
            log.error("ErrorNull={}", e.getMessage());
        }
        documentNewFarmerRepository.deleteById(Long.valueOf(data));
        documentResponse.setMsg(deleteSS);
        documentResponse.setStatusMsg(labelSuccess);
        documentResponse.setCodeTypeMsg(labelComplete);
        documentResponse.setDataId(Long.valueOf(data));


        log.info("successService=DocumentNewFarmerService.deleteNewFarmer");
        return documentResponse;
    }

    @Transactional
    public DocumentNewFarmer selectNewFarmerByDocId(String docId, String fac, String year) {
        log.info("initService=DocumentNewFarmerService.selectNewFarmerByDocId");
        log.info("successService=DocumentNewFarmerService.selectNewFarmerByDocId");
        return documentNewFarmerJDBCRepository.selectNewFarmerByDocId(docId, fac, year);
    }

    @Transactional
    public DocumentNewFarmer selectNewFarmerById(Long id) {
        log.info("initService=DocumentNewFarmerService.selectNewFarmerById");
        log.info("successService=DocumentNewFarmerService.selectNewFarmerById");
        return documentNewFarmerJDBCRepository.selectNewFarmerById(id);
    }
}
