package com.mpg.documentservice.service;


import com.mpg.documentservice.entity.*;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.repository.DocumentFarmerJDBCRepository;
import com.mpg.documentservice.repository.DocumentFarmerRepository;
import com.mpg.documentservice.util.DecodeBase64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;


@Service
@Slf4j
public class DocumentFarmerService {
    @Value("${pattern.time}")
    private String pattern;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${folder.idcard}")
    private String idCard;
    @Value("${folder.bookbank}")
    private String bookBank;
    @Value("${folder.companyseal}")
    private String companySeal;
    @Value("${folder.legalentity}")
    private String legalEntity;
    @Value("${folder.homebook}")
    private String homeBook;
    @Value("${folder.docimg}")
    private String docImg;
    @Value("${label.DELETESUCCESS}")
    private String deleteSS;

    @Autowired
    private DecodeBase64 decodeBase64;
    @Autowired
    private DocumentFarmerRepository documentFarmerRepository;
    @Autowired
    private DocumentFarmerJDBCRepository documentFarmerRepositoryJDBC;

    @Autowired
    private FlowService flowService;

    private static final String DATABEFOREUPDATE = "DataBeforeUpdate={}";
    private static final String DATEAFTERUPDATE = "DataAfterUpdate={}";

    @Transactional
    public DocumentResponse deleteFarmer(String data) {
        log.info("initService=DocumentFarmerService.deleteFarmer");
        DocumentResponse documentResponse = new DocumentResponse();
        try {
            flowService.terminateFlow("ITCANE",documentFarmerRepository.getOne(Long.valueOf(data)).getFlowProcessId().toString());
        }catch (NullPointerException e){
            log.error("ErrorNull={}",e.getMessage());
        }
        documentFarmerRepository.deleteById(Long.valueOf(data));


        documentResponse.setMsg(deleteSS);
        documentResponse.setStatusMsg(labelSuccess);
        documentResponse.setCodeTypeMsg(labelComplete);
        documentResponse.setDataId(Long.valueOf(data));


        log.info("successService=DocumentFarmerService.deleteFarmer");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse insertFarmer(DocumentFarmer data) {
        log.info("initService=DocumentFarmerService.insertFarmer");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() == null) {
            String msgWarning = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            data.setDocCreateDate(new Date());

            data.setDocId("MPFM" + simpleDateFormat.format(new Date()));
            data.setBpDateCreate(new Date());

            data.setDocIdRef(data.getDocId());
            data.setBpCodeDoc(data.getBpCode());
            try {
                String pathImg = decodeBase64.writeFile(data.getDocImage(), data.getDocFac(), data.getDocYear(), docImg);
                data.setDocImage(pathImg);
            } catch (NullPointerException e) {
                log.info("docImg is null");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), data.getDocFac(), data.getDocYear(), idCard);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), data.getDocFac(), data.getDocYear(), idCard);
                AttachIdCard attachIdCard = new AttachIdCard();
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(data.getDocId());
                data.setAttachIdCard(attachIdCard);
            } catch (NullPointerException e) {
                msgWarning += "[attachIdCard is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachIdCard is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachCompanySeal().getImgPath(), data.getDocFac(), data.getDocYear(), companySeal);
                AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
                attachCompanySeal.setImgPath(getImgPathPath);
                attachCompanySeal.setCreateTime(new Date());
                attachCompanySeal.setDocId(data.getDocId());
                data.setAttachCompanySeal(attachCompanySeal);
            } catch (NullPointerException e) {
                msgWarning += "[attachCompanySeal is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachCompanySeal is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachLegalEntity().getImgPath(), data.getDocFac(), data.getDocYear(), legalEntity);
                AttachLegalEntity attachLegalEntity = new AttachLegalEntity();
                attachLegalEntity.setImgPath(getImgPathPath);
                attachLegalEntity.setCreateTime(new Date());
                attachLegalEntity.setDocId(data.getDocId());
                data.setAttachLegalEntity(attachLegalEntity);
            } catch (NullPointerException e) {
                msgWarning += "[attachLegalEntity is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachLegalEntity is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), data.getDocFac(), data.getDocYear(), bookBank);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), data.getDocFac(), data.getDocYear(), bookBank);
                AttachBookBank attachBookBank = new AttachBookBank();
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(data.getDocId());
                data.setAttachBookBank(attachBookBank);
            } catch (NullPointerException e) {
                msgWarning += "[attachBookBank is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachBookBank is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), data.getDocFac(), data.getDocYear(), homeBook);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), data.getDocFac(), data.getDocYear(), homeBook);
                AttachHome attachHome = data.getAttachHome();
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(data.getDocId());
                data.setAttachHome(attachHome);
            } catch (NullPointerException e) {
                msgWarning += "[attachHome is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachHome is null]");
            }
            documentFarmerRepository.save(data);
            if (msgWarning.length() != 0) {
                documentResponse.setMsg(msgWarning);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelWarning);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg("Insert Success");
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            }
        } else {
            documentResponse.setMsg("Can't Insert Id");
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentFarmerService.insertFarmer");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateAttachHomeIdCardBookBank(DocumentFarmer data) {
        log.info("initService=DocumentFarmerService.updateAttachHomeIdCardBookBank");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {

            AttachHome attachHome = new AttachHome();
            AttachBookBank attachBookBank = new AttachBookBank();
            AttachIdCard attachIdCard = new AttachIdCard();
            DocumentFarmer documentFarmerRepositoryOne = documentFarmerRepository.getOne(data.getId());
            String getImgPathBackPath;
            String getImgPathFrontPath;
            log.info(DATABEFOREUPDATE, documentFarmerRepositoryOne.toString());
            try {
                log.info("modeUpdate Id={}", documentFarmerRepositoryOne.getAttachHome().getId());
                if (documentFarmerRepositoryOne.getAttachHome().getId() != null && data.getAttachHome().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), homeBook);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), homeBook);
                    attachHome = documentFarmerRepositoryOne.getAttachHome();
                    attachHome.setImgPathFront(getImgPathFrontPath);
                    attachHome.setImgPathBack(getImgPathBackPath);
                    attachHome.setCreateTime(new Date());
                    documentFarmerRepositoryOne.setAttachHome(attachHome);
                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), homeBook);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), homeBook);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(documentFarmerRepositoryOne.getDocId());
                documentFarmerRepositoryOne.setAttachHome(attachHome);
            }


            try {
                log.info("modeUpdate Id={}", documentFarmerRepositoryOne.getAttachIdCard().getId());

                if (documentFarmerRepositoryOne.getAttachIdCard().getId() != null && data.getAttachIdCard().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), idCard);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), idCard);
                    attachIdCard = documentFarmerRepositoryOne.getAttachIdCard();
                    attachIdCard.setImgPathFront(getImgPathFrontPath);
                    attachIdCard.setImgPathBack(getImgPathBackPath);
                    attachIdCard.setCreateTime(new Date());
                    documentFarmerRepositoryOne.setAttachIdCard(attachIdCard);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), idCard);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), idCard);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(documentFarmerRepositoryOne.getDocId());
                documentFarmerRepositoryOne.setAttachIdCard(attachIdCard);
            }

            try {
                log.info("modeUpdate Id={}", documentFarmerRepositoryOne.getAttachBookBank().getId());

                if (documentFarmerRepositoryOne.getAttachBookBank().getId() != null && data.getAttachBookBank().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), bookBank);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), bookBank);
                    attachBookBank = documentFarmerRepositoryOne.getAttachBookBank();
                    attachBookBank.setImgPathFront(getImgPathFrontPath);
                    attachBookBank.setImgPathBack(getImgPathBackPath);
                    attachBookBank.setCreateTime(new Date());
                    documentFarmerRepositoryOne.setAttachBookBank(attachBookBank);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), bookBank);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), documentFarmerRepositoryOne.getDocFac(), documentFarmerRepositoryOne.getDocYear(), bookBank);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(documentFarmerRepositoryOne.getDocId());
                documentFarmerRepositoryOne.setAttachBookBank(attachBookBank);
            }
            documentFarmerRepository.save(documentFarmerRepositoryOne);
            log.info(DATEAFTERUPDATE, documentFarmerRepositoryOne.toString());
            documentResponse.setMsg(labelUpSS);
            documentResponse.setStatusMsg(labelSuccess);
            documentResponse.setCodeTypeMsg(labelComplete);
            documentResponse.setDocId(data.getDocId());
            documentResponse.setDataId(data.getId());


        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentFarmerService.updateAttachHomeIdCardBookBank");
        return documentResponse;
    }


    @Transactional
    public DocumentFarmer selectFarmerByDocId(String docId, String fac, String year) {
        log.info("initService=DocumentFarmerService.selectFarmerByDocId");
        log.info("successService=DocumentFarmerService.selectFarmerByDocId");
        return documentFarmerRepositoryJDBC.selectFarmerByDocId(docId, fac, year);
    }

    @Transactional
    public DocumentFarmer selectFarmerById(Long id) {
        log.info("initService=DocumentFarmerService.selectFarmerById");
        log.info("successService=DocumentFarmerService.selectFarmerById");
        return documentFarmerRepositoryJDBC.selectFarmerById(id);
    }

}
