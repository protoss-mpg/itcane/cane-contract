package com.mpg.documentservice.service;

import com.mpg.documentservice.entity.*;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.repository.DocumentNewVenderJDBCRepository;
import com.mpg.documentservice.repository.DocumentVenderRepository;
import com.mpg.documentservice.util.DecodeBase64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class DocumentNewVenderService {
    @Value("${pattern.time}")
    private String pattern;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${folder.idcard}")
    private String idCard;
    @Value("${folder.bookbank}")
    private String bookBank;
    @Value("${folder.companyseal}")
    private String companySeal;
    @Value("${folder.legalentity}")
    private String legalEntity;
    @Value("${folder.homebook}")
    private String homeBook;
    @Value("${folder.docimg}")
    private String docImg;
    @Value("${label.DELETESUCCESS}")
    private String deleteSS;

    @Autowired
    private FlowService flowService;

    @Autowired
    private DecodeBase64 decodeBase64;

    @Autowired
    private DocumentVenderRepository documentVenderRepository;
    @Autowired
    private DocumentNewVenderJDBCRepository documentNewVenderJDBCRepository;

    private static final String DATABEFOREUPDATE = "DataBeforeUpdate={}";
    private static final String DATEAFTERUPDATE = "DataAfterUpdate={}";

    @Transactional
    public DocumentResponse insertNewVender(DocumentNewVender data) {
        log.info("initService=DocumentNewVenderService.insertNewVender");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() == null) {
            String msgWarning = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            data.setDocCreateDate(new Date());
            try {

                String pathImg = decodeBase64.writeFile(data.getDocImage(), data.getDocFac(), data.getDocYear(), docImg);
                data.setDocImage(pathImg);
            } catch (NullPointerException e) {
                log.info("docImg is null");
            }
            try {
                String pathImg = decodeBase64.writeFile(data.getBpImage(), data.getDocFac(), data.getDocYear(), "bpImage");
                data.setBpImage(pathImg);
            } catch (NullPointerException e) {
                log.info("BpImage is null");
            }
            data.setDocId("MPFM" + simpleDateFormat.format(new Date()));
            data.setBpDateCreate(new Date());
            data.setDocIdRef(data.getDocId());
            data.setBpCodeDoc(data.getBpCode());

            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), data.getDocFac(), data.getDocYear(), idCard);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), data.getDocFac(), data.getDocYear(), idCard);
                AttachIdCard attachIdCard = new AttachIdCard();
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(data.getDocId());
                data.setAttachIdCard(attachIdCard);
            } catch (NullPointerException e) {
                msgWarning += "[attachIdCard is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachIdCard is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachCompanySeal().getImgPath(), data.getDocFac(), data.getDocYear(), companySeal);
                AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
                attachCompanySeal.setImgPath(getImgPathPath);
                attachCompanySeal.setCreateTime(new Date());
                attachCompanySeal.setDocId(data.getDocId());
                data.setAttachCompanySeal(attachCompanySeal);
            } catch (NullPointerException e) {
                msgWarning += "[attachCompanySeal is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachCompanySeal is null]");
            }
            try {
                String getImgPathPath = decodeBase64.writeFile(data.getAttachLegalEntity().getImgPath(), data.getDocFac(), data.getDocYear(), legalEntity);
                AttachLegalEntity attachLegalEntity = new AttachLegalEntity();
                attachLegalEntity.setImgPath(getImgPathPath);
                attachLegalEntity.setCreateTime(new Date());
                attachLegalEntity.setDocId(data.getDocId());
                data.setAttachLegalEntity(attachLegalEntity);
            } catch (NullPointerException e) {
                msgWarning += "[attachLegalEntity is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachLegalEntity is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), data.getDocFac(), data.getDocYear(), bookBank);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), data.getDocFac(), data.getDocYear(), bookBank);
                AttachBookBank attachBookBank = new AttachBookBank();
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(data.getDocId());
                data.setAttachBookBank(attachBookBank);
            } catch (NullPointerException e) {
                msgWarning += "[attachBookBank is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachBookBank is null]");
            }
            try {
                String getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), data.getDocFac(), data.getDocYear(), homeBook);
                String getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), data.getDocFac(), data.getDocYear(), homeBook);
                AttachHome attachHome = data.getAttachHome();
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(data.getDocId());
                data.setAttachHome(attachHome);
            } catch (NullPointerException e) {
                msgWarning += "[attachHome is null]";
                log.warn("DocumentNewFarmerService.insertNewFarmer.[attachHome is null]");
            }
            documentVenderRepository.save(data);
            if (msgWarning.length() != 0) {
                documentResponse.setMsg(msgWarning);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelWarning);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg(labelInsSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            }
        } else {
            documentResponse.setMsg("Can't Insert Id");
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }

        log.info("successService=DocumentNewVenderService.insertNewVender");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse deleteNewVender(String data) {
        log.info("initService=DocumentNewVenderService.deleteNewVender");
        DocumentResponse documentResponse = new DocumentResponse();
        try {
            flowService.terminateFlow("ITCANE", documentVenderRepository.getOne(Long.valueOf(data)).getFlowProcessId().toString());
        } catch (NullPointerException e) {
            log.error("ErrorNull={}", e.getMessage());
        }
        documentVenderRepository.deleteById(Long.valueOf(data));
        documentResponse.setMsg(deleteSS);
        documentResponse.setStatusMsg(labelSuccess);
        documentResponse.setCodeTypeMsg(labelComplete);
        documentResponse.setDataId(Long.valueOf(data));


        log.info("successService=DocumentNewVenderService.deleteNewVender");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateAttachFile(DocumentNewVender data) {
        log.info("initService=DocumentNewVenderService.updateAttachFile");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {

            AttachHome attachHome = new AttachHome();
            AttachBookBank attachBookBank = new AttachBookBank();
            AttachIdCard attachIdCard = new AttachIdCard();
            AttachCompanySeal attachCompanySeal = new AttachCompanySeal();
            AttachLegalEntity attachLegalEntity = new AttachLegalEntity();

            DocumentNewVender documentNewVenderRepositoryOne = documentVenderRepository.getOne(data.getId());
            String getImgPathBackPath = "";
            String getImgPathFrontPath = "";
            log.info(DATABEFOREUPDATE, documentNewVenderRepositoryOne.toString());
            try {
                log.info("modeUpdate Id={}", documentNewVenderRepositoryOne.getAttachHome().getId());
                if (documentNewVenderRepositoryOne.getAttachCompanySeal().getId() != null && data.getAttachCompanySeal().getImgPath() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachCompanySeal().getImgPath(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), companySeal);
                    attachCompanySeal = documentNewVenderRepositoryOne.getAttachCompanySeal();
                    attachCompanySeal.setImgPath(getImgPathBackPath);
                    attachCompanySeal.setCreateTime(new Date());
                    documentNewVenderRepositoryOne.setAttachCompanySeal(attachCompanySeal);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                try {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachCompanySeal().getImgPath(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), companySeal);
                } catch (NullPointerException ee) {
                    log.error("ErrorNull={}", ee);
                }
                attachCompanySeal.setImgPath(getImgPathBackPath);
                attachCompanySeal.setCreateTime(new Date());
                attachCompanySeal.setDocId(documentNewVenderRepositoryOne.getDocId());
                documentNewVenderRepositoryOne.setAttachCompanySeal(attachCompanySeal);
            }


            try {
                log.info("modeUpdate Id={}", documentNewVenderRepositoryOne.getAttachLegalEntity().getId());
                if (documentNewVenderRepositoryOne.getAttachLegalEntity().getId() != null && data.getAttachLegalEntity().getImgPath() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachLegalEntity().getImgPath(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), legalEntity);
                    attachLegalEntity = documentNewVenderRepositoryOne.getAttachLegalEntity();
                    attachLegalEntity.setImgPath(getImgPathBackPath);
                    attachLegalEntity.setCreateTime(new Date());
                    documentNewVenderRepositoryOne.setAttachLegalEntity(attachLegalEntity);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                try {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachLegalEntity().getImgPath(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), legalEntity);
                } catch (NullPointerException ee) {
                    log.error("ErrorNull={}", ee);
                }
                attachLegalEntity.setImgPath(getImgPathBackPath);
                attachLegalEntity.setCreateTime(new Date());
                attachLegalEntity.setDocId(documentNewVenderRepositoryOne.getDocId());
                documentNewVenderRepositoryOne.setAttachLegalEntity(attachLegalEntity);
            }


            try {
                log.info("modeUpdate Id={}", documentNewVenderRepositoryOne.getAttachHome().getId());
                if (documentNewVenderRepositoryOne.getAttachHome().getId() != null && data.getAttachHome().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), homeBook);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), homeBook);
                    attachHome = documentNewVenderRepositoryOne.getAttachHome();
                    attachHome.setImgPathFront(getImgPathFrontPath);
                    attachHome.setImgPathBack(getImgPathBackPath);
                    attachHome.setCreateTime(new Date());
                    documentNewVenderRepositoryOne.setAttachHome(attachHome);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachHome().getImgPathBack(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), homeBook);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachHome().getImgPathFront(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), homeBook);
                attachHome.setImgPathBack(getImgPathBackPath);
                attachHome.setImgPathFront(getImgPathFrontPath);
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(documentNewVenderRepositoryOne.getDocId());
                documentNewVenderRepositoryOne.setAttachHome(attachHome);
            }


            try {
                log.info("modeUpdate Id={}", documentNewVenderRepositoryOne.getAttachIdCard().getId());

                if (documentNewVenderRepositoryOne.getAttachIdCard().getId() != null && data.getAttachIdCard().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), idCard);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), idCard);
                    attachIdCard = documentNewVenderRepositoryOne.getAttachIdCard();
                    attachIdCard.setImgPathFront(getImgPathFrontPath);
                    attachIdCard.setImgPathBack(getImgPathBackPath);
                    attachIdCard.setCreateTime(new Date());
                    documentNewVenderRepositoryOne.setAttachIdCard(attachIdCard);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathBack(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), idCard);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachIdCard().getImgPathFront(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), idCard);
                attachIdCard.setImgPathBack(getImgPathBackPath);
                attachIdCard.setImgPathFront(getImgPathFrontPath);
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(documentNewVenderRepositoryOne.getDocId());
                documentNewVenderRepositoryOne.setAttachIdCard(attachIdCard);
            }

            try {
                log.info("modeUpdate Id={}", documentNewVenderRepositoryOne.getAttachBookBank().getId());

                if (documentNewVenderRepositoryOne.getAttachBookBank().getId() != null && data.getAttachBookBank().getImgPathFront() != null) {
                    getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), bookBank);
                    getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), bookBank);
                    attachBookBank = documentNewVenderRepositoryOne.getAttachBookBank();
                    attachBookBank.setImgPathFront(getImgPathFrontPath);
                    attachBookBank.setImgPathBack(getImgPathBackPath);
                    attachBookBank.setCreateTime(new Date());
                    documentNewVenderRepositoryOne.setAttachBookBank(attachBookBank);

                }
            } catch (NullPointerException e) {
                log.info("modeInsert");
                getImgPathBackPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathBack(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), bookBank);
                getImgPathFrontPath = decodeBase64.writeFile(data.getAttachBookBank().getImgPathFront(), documentNewVenderRepositoryOne.getDocFac(), documentNewVenderRepositoryOne.getDocYear(), bookBank);
                attachBookBank.setImgPathBack(getImgPathBackPath);
                attachBookBank.setImgPathFront(getImgPathFrontPath);
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(documentNewVenderRepositoryOne.getDocId());
                documentNewVenderRepositoryOne.setAttachBookBank(attachBookBank);
            }
            documentVenderRepository.save(documentNewVenderRepositoryOne);
            log.info(DATEAFTERUPDATE, documentNewVenderRepositoryOne.toString());
            documentResponse.setMsg(labelUpSS);
            documentResponse.setStatusMsg(labelSuccess);
            documentResponse.setCodeTypeMsg(labelComplete);
            documentResponse.setDocId(data.getDocId());
            documentResponse.setDataId(data.getId());


        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentNewVenderService.updateAttachFile");
        return documentResponse;
    }

    @Transactional
    public DocumentNewVender selectNewVenderByDocId(String docId, String fac, String year) {
        log.info("initService=DocumentNewVenderService.selectNewVenderByDocId");
        log.info("successService=DocumentNewVenderService.selectNewVenderByDocId");
        return documentNewVenderJDBCRepository.selectNewVenderByDocId(docId, fac, year);
    }

    @Transactional
    public DocumentNewVender selectNewVenderById(Long id) {
        log.info("initService=DocumentNewVenderService.selectNewVenderById");
        log.info("successService=DocumentNewVenderService.selectNewVenderById");
        return documentNewVenderJDBCRepository.selectNewVenderByDocId(id);
    }
}
