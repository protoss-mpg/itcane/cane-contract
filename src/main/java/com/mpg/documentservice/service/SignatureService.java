package com.mpg.documentservice.service;

import com.mpg.documentservice.entity.SignatureEntity;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.repository.SignatureEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class SignatureService {
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${label.DELETESUCCESS}")
    private String deleteSS;

    @Autowired
    private SignatureEntityRepository signatureEntityRepository;

    @Transactional
    public DocumentResponse insertSignature(SignatureEntity data) {
        log.info("initService=SignatureService.insertSignature");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() == null) {
            data.setCreateDate(new Date());
            data.setCreateBy(data.getFullName());
            signatureEntityRepository.save(data);
            documentResponse.setStatusMsg(labelSuccess);
            documentResponse.setCodeTypeMsg(labelComplete);
            documentResponse.setMsg(labelInsSS);
            documentResponse.setDataId(data.getId());
        } else {
            documentResponse.setMsg("Can't Insert Id");
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=SignatureService.insertSignature");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateSignature(SignatureEntity data) {
        log.info("initService=SignatureService.updateSignature");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {
            try {
                SignatureEntity signatureEntityRepositoryOne = signatureEntityRepository.getOne(data.getId());

                signatureEntityRepositoryOne.setFac(data.getFac());
                signatureEntityRepositoryOne.setUserId(data.getUserId());
                signatureEntityRepositoryOne.setPreName(data.getPreName());
                signatureEntityRepositoryOne.setName(data.getName());
                signatureEntityRepositoryOne.setLName(data.getLName());
                signatureEntityRepositoryOne.setFullName(data.getFullName());
                signatureEntityRepositoryOne.setEmail(data.getEmail());
                signatureEntityRepositoryOne.setSignature(data.getSignature());

                signatureEntityRepositoryOne.setUpdateDate(new Date());
                signatureEntityRepositoryOne.setUpdateBy(data.getFullName());
                signatureEntityRepository.save(signatureEntityRepositoryOne);

                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setMsg(labelUpSS);
                documentResponse.setDataId(signatureEntityRepositoryOne.getId());
            } catch (Exception e) {
                documentResponse.setMsg(e.getMessage());
                documentResponse.setStatusMsg(labelError);
                documentResponse.setCodeTypeMsg(labelError);
            }
        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=SignatureService.updateSignature");
        return documentResponse;
    }



    @Transactional
    public DocumentResponse deleteSignature(String data) {
        log.info("initService=SignatureService.deleteSignature");
        DocumentResponse documentResponse = new DocumentResponse();

        signatureEntityRepository.deleteById(Long.valueOf(data));
        documentResponse.setMsg(deleteSS);
        documentResponse.setStatusMsg(labelSuccess);
        documentResponse.setCodeTypeMsg(labelComplete);
        documentResponse.setDataId(Long.valueOf(data));

        log.info("successService=SignatureService.deleteSignature");
        return documentResponse;
    }

    @Transactional
    public Optional<SignatureEntity> selectSignatureById(Long id) {
        log.info("initService=SignatureService.selectSignatureById");
        log.info("successService=SignatureService.selectSignatureById");
        return signatureEntityRepository.findById(id);
    }
    @Transactional
    public Optional<SignatureEntity> selectSignatureByUserId(String id) {
        log.info("initService=SignatureService.selectSignatureById");
        log.info("successService=SignatureService.selectSignatureById");
        return signatureEntityRepository.findByUserId(id);
    }
}
