package com.mpg.documentservice.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mpg.documentservice.entity.*;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.repository.DocumentPromiseJDBCRepository;
import com.mpg.documentservice.repository.DocumentPromiseRepository;
import com.mpg.documentservice.repository.FactoryConfigJDBCRepository;
import com.mpg.documentservice.util.DecodeBase64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class DocumentPromiseService {
    @Value("${pattern.time}")
    private String pattern;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${folder.idcard}")
    private String idCard;
    @Value("${folder.bookbank}")
    private String bookBank;
    @Value("${folder.companyseal}")
    private String companySeal;
    @Value("${folder.legalentity}")
    private String legalEntity;
    @Value("${folder.homebook}")
    private String homeBook;
    @Value("${folder.docimg}")
    private String docImg;
    @Value("${label.DELETESUCCESS}")
    private String deleteSS;
    @Value("${url.iservice}")
    private String urlIService;
    @Autowired
    private DecodeBase64 decodeBase64;

    @Autowired
    private DocumentPromiseRepository documentPromiseRepository;
    @Autowired
    private DocumentPromiseJDBCRepository documentPromiseJDBCRepository;
    @Autowired
    private FactoryConfigJDBCRepository factoryConfigJDBCRepository;

    private RestTemplate restTemplate;

    @Autowired
    private FlowService flowService;

    @Transactional
    public DocumentResponse insertPromise(DocumentPromise data) {
        log.info("initService=DocumentPromiseService.insertPromise");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() == null) {
            String msgWarning = "";

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            data.setDocId("MPFM" + simpleDateFormat.format(new Date()));

            data.setDocCreateDate(new Date());

            data.setDocCreateDate(new Date());

            data.setDocIdRef(data.getDocId());
            data.setBpCodeDoc(data.getBpCode());
            try {
                String pathImg = decodeBase64.writeFile(data.getDocImage(), data.getDocFac(), data.getDocYear(), docImg);
                data.setDocImage(pathImg);
            } catch (NullPointerException e) {
                log.info("docImg is null");
            }
            try {
                AttachIdCard attachIdCard = data.getAttachIdCard();
                attachIdCard.setCreateTime(new Date());
                attachIdCard.setDocId(data.getDocId());
                data.setAttachIdCard(attachIdCard);
            } catch (NullPointerException e) {
                msgWarning += "[attachIdCard is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachIdCard is null]");
            }
            try {
                AttachCompanySeal attachCompanySeal = data.getAttachCompanySeal();
                attachCompanySeal.setCreateTime(new Date());
                attachCompanySeal.setDocId(data.getDocId());
                data.setAttachCompanySeal(attachCompanySeal);
            } catch (NullPointerException e) {
                msgWarning += "[attachCompanySeal is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachCompanySeal is null]");
            }
            try {
                AttachLegalEntity attachLegalEntity = data.getAttachLegalEntity();
                attachLegalEntity.setCreateTime(new Date());
                attachLegalEntity.setDocId(data.getDocId());
                data.setAttachLegalEntity(attachLegalEntity);
            } catch (NullPointerException e) {
                msgWarning += "[attachLegalEntity is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachLegalEntity is null]");
            }
            try {
                AttachBookBank attachBookBank = data.getAttachBookBank();
                attachBookBank.setCreateTime(new Date());
                attachBookBank.setDocId(data.getDocId());
                data.setAttachBookBank(attachBookBank);
            } catch (NullPointerException e) {
                msgWarning += "[attachBookBank is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachBookBank is null]");
            }
            try {
                AttachHome attachHome = data.getAttachHome();
                attachHome.setCreateTime(new Date());
                attachHome.setDocId(data.getDocId());
                data.setAttachHome(attachHome);
            } catch (NullPointerException e) {
                msgWarning += "[attachHome is null]";
                log.warn("DocumentFarmerService.insertFarmer.[attachHome is null]");
            }

            documentPromiseRepository.save(data);
            if (msgWarning.length() != 0) {
                documentResponse.setMsg(msgWarning);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelWarning);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg("Insert Success");
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            }
        } else {
            documentResponse.setMsg("Can't Insert Id");
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentPromiseService.insertPromise");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse deletePromise(String data) {
        log.info("initService=DocumentPromiseService.deletePromise");
        DocumentResponse documentResponse = new DocumentResponse();
        try {
            flowService.terminateFlow("ITCANE", documentPromiseRepository.getOne(Long.valueOf(data)).getFlowProcessId().toString());
        } catch (NullPointerException e) {
            log.error("ErrorNull={}", e.getMessage());
        }
        documentPromiseRepository.deleteById(Long.valueOf(data));
        documentResponse.setMsg(deleteSS);
        documentResponse.setStatusMsg(labelSuccess);
        documentResponse.setCodeTypeMsg(labelComplete);
        documentResponse.setDataId(Long.valueOf(data));


        log.info("successService=DocumentPromiseService.deletePromise");
        return documentResponse;
    }

    @Transactional
    public DocumentPromise selectPromiseByDocId(String docId, String fac, String year) {
        log.info("initService=DocumentPromiseService.selectPromiseByDocId");
        JsonObject resIService;
        JsonArray resIServiceList;
        JsonObject finalData;
        restTemplate = new RestTemplate();
        DocumentPromise documentPromise = documentPromiseJDBCRepository.selectPromiseByDocId(docId, fac, year);
        FactoryConfig factoryConfig = factoryConfigJDBCRepository.selectFacByFacCode(fac);

        for (int i = 0; i < 10; i++) {
            String yearByDocId = "6" + (3 - i) + "6" + (4 - i);
            log.info("year={}", yearByDocId);
            String iServicePath = urlIService + factoryConfig.getFacCode() + "/" + yearByDocId + "/" + documentPromise.getBpCode();

            try {
                URI uriIServicePath = new URI(iServicePath);
                Gson g = new Gson();
                resIService = g.fromJson(restTemplate.getForObject(uriIServicePath, String.class), JsonObject.class);

                resIServiceList = resIService.get("results").getAsJsonArray();
                finalData = resIServiceList.get(0).getAsJsonObject();
                if (resIServiceList.size() != 0) {
                    Object oFinalData = g.fromJson(finalData, Object.class);

                    documentPromise.setBpPreName(finalData.get("bpTitle").toString());
                    documentPromise.setBpFName(finalData.get("bpFirstName").toString());
                    documentPromise.setBpLName(finalData.get("bpLastName").toString());
                    documentPromise.setBpName(finalData.get("bpTitle").toString() + " " + finalData.get("bpLastName").toString() + "  " + finalData.get("bpLastName").toString());
                    documentPromise.setZoneId(finalData.get("zoneCode").toString());
                    documentPromise.setSubZone(finalData.get("subZoneCode").toString());
                    documentPromise.setResArray(oFinalData);
                    break;
                }

            } catch (URISyntaxException e) {
                log.error("UriError={}", e.getMessage());
            }
        }

        log.info("successService=DocumentPromiseService.selectPromiseByDocId");
        return documentPromise;
    }

    @Transactional
    public DocumentPromise selectPromiseById(Long id) {
        log.info("initService=DocumentPromiseService.selectPromiseById");
        JsonObject resIService;
        JsonArray resIServiceList;
        JsonObject finalData;
        restTemplate = new RestTemplate();
        DocumentPromise documentPromise = documentPromiseJDBCRepository.selectPromiseByDocId(id);
        FactoryConfig factoryConfig = factoryConfigJDBCRepository.selectFacByFacCode(documentPromise.getDocFac());

        for (int i = 0; i < 10; i++) {
            String yearById = "6" + (3 - i) + "6" + (4 - i);
            log.info("year={}", yearById);
            String iServicePath = urlIService + factoryConfig.getFacCode() + "/" + yearById + "/" + documentPromise.getBpCode();

            try {
                URI uriIServicePath = new URI(iServicePath);
                Gson g = new Gson();
                resIService = g.fromJson(restTemplate.getForObject(uriIServicePath, String.class), JsonObject.class);
                resIServiceList = resIService.get("results").getAsJsonArray();
                if (resIServiceList.size() != 0) {
                    finalData = resIServiceList.get(0).getAsJsonObject();
                    documentPromise.setBpPreName(finalData.get("bpTitle").toString());
                    documentPromise.setBpFName(finalData.get("bpFirstName").toString());
                    documentPromise.setBpLName(finalData.get("bpLastName").toString());
                    documentPromise.setBpName(finalData.get("bpTitle").toString() + " " + finalData.get("bpLastName").toString() + "  " + finalData.get("bpLastName").toString());
                    documentPromise.setZoneId(finalData.get("zoneCode").toString());
                    documentPromise.setSubZone(finalData.get("subZoneCode").toString());
                    Object oFinalData = g.fromJson(finalData, Object.class);
                    documentPromise.setResArray(oFinalData);
                    break;
                }

            } catch (URISyntaxException e) {
                log.error("UriError={}", e.getMessage());
            }
        }
        log.info("successService=DocumentPromiseService.selectPromiseById");
        return documentPromise;
    }
}
