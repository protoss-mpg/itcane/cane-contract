package com.mpg.documentservice.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mpg.documentservice.entity.DocumentMain;
import com.mpg.documentservice.model.DocumentResponse;
import com.mpg.documentservice.repository.DocumentMainRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Service
@Slf4j
public class DocumentMainService {
    @Value("${pattern.time}")
    private String pattern;
    @Value("${label.SUCCESS}")
    private String labelSuccess;
    @Value("${label.ERROR}")
    private String labelError;
    @Value("${label.WARNING}")
    private String labelWarning;
    @Value("${label.COMPLETE}")
    private String labelComplete;
    @Value("${label.UPDATESUCCESS}")
    private String labelUpSS;
    @Value("${label.INSERTSUCCESS}")
    private String labelInsSS;
    @Value("${label.ERRORREQID}")
    private String labelReqId;
    @Value("${url.adminservice}")
    private String urlAdmin;

    @Autowired
    private DocumentMainRepository documentMainRepository;

    private static final String DATABEFOREUPDATE = "DataBeforeUpdate={}";
    private static final String DATEAFTERUPDATE = "DataAfterUpdate={}";

    private RestTemplate restTemplate;

    @Autowired
    private SignatureService signatureService;

    @Autowired
    private FlowService flowService;


    @Transactional
    public DocumentResponse updateTrueStatusAppove(DocumentMain data) {
        log.info("initService=DocumentMainService.updateTrueStatusAppove");
        DocumentResponse documentResponse = new DocumentResponse();
        DocumentMain documentMain = documentMainRepository.getOne(data.getId());
        if (documentMain.getStatusAppove().equals("N")){
            documentMain.setStatusAppove("Y");
            documentMainRepository.save(documentMain);
            documentResponse.setMsg(labelUpSS);
            documentResponse.setStatusMsg(labelSuccess);
            documentResponse.setCodeTypeMsg(labelComplete);
            documentResponse.setDocId(documentMain.getDocId());
            documentResponse.setDataId(documentMain.getId());
        }
        else {
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMainService.updateTrueStatusAppove");
        return documentResponse;
    }

    @Transactional
    public List<DocumentMain> selectByDocId(String id) {
        log.info("initService=DocumentMainService.selectByDocId");
        List<DocumentMain> documentMains = new ArrayList();
        DocumentMain documentMain;
        String[] splitted = id.split(",");

        for (int i = 0; i < splitted.length; i++) {

            String[] resFinal = splitted[i].split("-");

            documentMain = documentMainRepository.getOne(Long.parseLong(resFinal[0]));
            documentMain.setProcessId(resFinal[1]);
            documentMain.setTaskId(resFinal[2]);

            documentMains.add(documentMain);
        }

        return documentMains;
    }

    @Transactional
    public DocumentResponse updateSignFarmer(DocumentMain data) {
        log.info("initService=DocumentMainService.updateSignFarmer");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {
            String picBase64;
            String farmerName;
            try {
                DocumentMain documentNewFarmerOptional = documentMainRepository.getOne(data.getId());
                log.info(DATABEFOREUPDATE, documentNewFarmerOptional.toString());
                picBase64 = data.getSignatureFarmerPic();
                farmerName = data.getSignatureFarmerName();

                documentNewFarmerOptional.setSignatureFarmerPic(picBase64);
                documentNewFarmerOptional.setSignatureFarmerDate(new Date());
                documentNewFarmerOptional.setSignatureFarmerName(farmerName);
                documentMainRepository.save(documentNewFarmerOptional);
                log.info(DATEAFTERUPDATE, data.toString());

                documentResponse.setMsg(labelUpSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } catch (Exception e) {
                documentResponse.setMsg(e.getMessage());
                documentResponse.setStatusMsg(labelError);
                documentResponse.setCodeTypeMsg(labelError);
            }
        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMainService.updateSignFarmer");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateSignPersonal(DocumentMain data) {
        log.info("initService=DocumentMainService.updateSignPersonal");
        DocumentResponse documentResponse = new DocumentResponse();

        if (data.getId() != null) {
            String picBase64;
            String farmerName;
            try {
                DocumentMain documentNewFarmerOptional = documentMainRepository.getOne(data.getId());
                log.info(DATABEFOREUPDATE, documentNewFarmerOptional.toString());

                picBase64 = data.getSignaturePersonalPic();
                farmerName = data.getSignaturePersonalName();

                documentNewFarmerOptional.setSignaturePersonalPic(picBase64);
                documentNewFarmerOptional.setSignaturePersonalDate(new Date());
                documentNewFarmerOptional.setSignaturePersonalName(farmerName);

                documentMainRepository.save(documentNewFarmerOptional);
                log.info(DATEAFTERUPDATE, data.toString());
                updateSignApproveAll(documentNewFarmerOptional);
                String adminPath = urlAdmin + "roleapprovegis/api/v1/" + documentNewFarmerOptional.getDocFac() + "/" + documentNewFarmerOptional.getDocCreateId();
                URI uriAdminPath = new URI(adminPath);
                Gson g = new Gson();
                JsonArray jsonArray = g.fromJson(restTemplate.getForObject(uriAdminPath, String.class), JsonArray.class);
                JsonObject yourJson = jsonArray.get(0).getAsJsonObject();

                List<Map<String, Object>> appove = new ArrayList();

                Map<String, Object> objectMap1 = new HashMap();
                objectMap1.put("sequence", 1);
                objectMap1.put("usernameApprover", yourJson.get("headzoneId").getAsString());
                objectMap1.put("roleFlow", "VRF");
                objectMap1.put("userId", yourJson.get("headzoneId").getAsString());
                objectMap1.put("email", yourJson.get("headzoneEmail").getAsString());
                appove.add(objectMap1);

//                Map<String, Object> objectMap2 = new HashMap();
//                objectMap2.put("sequence", 2);
//                objectMap2.put("usernameApprover", yourJson.get("managerFullName").getAsString());
//                objectMap2.put("roleFlow", "VRF");
//                objectMap2.put("userId", yourJson.get("managerId").getAsString());
//                objectMap2.put("email", yourJson.get("managerEmail").getAsString());
//                appove.add(objectMap2);

                Map<String, Object> objectMap3 = new HashMap();
                objectMap3.put("sequence", 2);
                objectMap3.put("usernameApprover", yourJson.get("directorId").getAsString());
                objectMap3.put("roleFlow", "APR");
                objectMap3.put("userId", yourJson.get("directorId").getAsString());
                objectMap3.put("email", yourJson.get("directorEmail").getAsString());
                appove.add(objectMap3);


                Boolean chkFlow = flowService.createFlow(documentNewFarmerOptional.getId(), documentNewFarmerOptional.getDocStatus(), appove, yourJson.get("subzoneEmail").getAsString().split("@")[0], yourJson.get("subzoneEmail").getAsString());
                if (chkFlow == false) {
                    throw new Exception(
                            "FlowService Return False"
                    );
                }
                documentResponse.setMsg(labelUpSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } catch (Exception e) {
                documentResponse.setMsg(e.getMessage());
                documentResponse.setStatusMsg(labelError);
                documentResponse.setCodeTypeMsg(labelError);
            }

        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMainService.updateSignPersonal");
        return documentResponse;
    }


    @Transactional
    public DocumentResponse updatePDF(DocumentMain data) {
        log.info("initService=DocumentMainService.updatePDF");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {

            try {
                DocumentMain documentNewFarmerOptional = documentMainRepository.getOne(data.getId());
                log.info(DATABEFOREUPDATE, documentNewFarmerOptional.toString());

                documentNewFarmerOptional.setStatusPdf("Y");
                documentNewFarmerOptional.setPathPdf(data.getPathPdf());
                documentMainRepository.save(documentNewFarmerOptional);
                log.info(DATEAFTERUPDATE, data.toString());

                documentResponse.setMsg(labelUpSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } catch (Exception e) {
                documentResponse.setMsg(e.getMessage());
                documentResponse.setStatusMsg(labelError);
                documentResponse.setCodeTypeMsg(labelError);
            }
        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMainService.updatePDF");
        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateSignApproveNotDirector(DocumentMain data) {
        log.info("initService=DocumentMainService.updateSignNotDirector");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {
            restTemplate = new RestTemplate();
            JsonArray roleApproveGisList = new JsonArray();
            DocumentMain documentMain = documentMainRepository.getOne(data.getId());
            String adminPath = urlAdmin + "roleapprovegis/api/v1/" + documentMain.getDocFac() + "/" + documentMain.getDocCreateId();
            URI uriAdminPath = null;

            try {
                uriAdminPath = new URI(adminPath);
                Gson g = new Gson();
                roleApproveGisList = g.fromJson(restTemplate.getForObject(uriAdminPath, String.class), JsonArray.class);
            } catch (URISyntaxException e) {
                log.error("ErrorURI={}", e.getMessage());
            }

            if (roleApproveGisList.size() != 0) {
                JsonObject roleApproveGisJson = roleApproveGisList.get(0).getAsJsonObject();
                documentMain.setSignatureHeadzoneId(roleApproveGisJson.get("headzoneId").getAsString());
                documentMain.setSignatureHeadzoneDate(new Date());
                documentMain.setSignatureManagerId(roleApproveGisJson.get("managerId").getAsString());
                documentMain.setSignatureManagerDate(new Date());
                documentMain.setStatusAppove("N");
                documentMainRepository.save(documentMain);
                documentResponse.setMsg(labelUpSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg("Nodata In tbl_prosonal");
                documentResponse.setStatusMsg(labelError);
                documentResponse.setCodeTypeMsg(labelError);
            }
        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMainService.updateSignNotDirector");

        return documentResponse;
    }

    @Transactional
    public DocumentResponse updateSignApproveAll(DocumentMain data) {
        log.info("initService=DocumentMainService.updateSignApproveAll");
        DocumentResponse documentResponse = new DocumentResponse();
        if (data.getId() != null) {
            restTemplate = new RestTemplate();
            JsonArray roleApproveGisList = new JsonArray();
            DocumentMain documentMain = documentMainRepository.getOne(data.getId());
            String adminPath = urlAdmin + "roleapprovegis/api/v1/" + documentMain.getDocFac() + "/" + documentMain.getDocCreateId();
            URI uriAdminPath;
            try {
                uriAdminPath = new URI(adminPath);
                Gson g = new Gson();
                roleApproveGisList = g.fromJson(restTemplate.getForObject(uriAdminPath, String.class), JsonArray.class);
            } catch (URISyntaxException e) {
                log.error("ErrorURI={}", e.getMessage());
            }
            if (roleApproveGisList.size() != 0) {
                JsonObject roleApproveGisJson = roleApproveGisList.get(0).getAsJsonObject();
                documentMain.setSignatureHeadzoneId(roleApproveGisJson.get("headzoneId").getAsString());
                documentMain.setSignatureHeadzoneDate(new Date());
                documentMain.setSignatureManagerId(roleApproveGisJson.get("managerId").getAsString());
                documentMain.setSignatureManagerDate(new Date());
                documentMain.setSignatureDirectorId(roleApproveGisJson.get("directorId").getAsString());
                documentMain.setSignatureDirectorDate(new Date());
                documentMain.setStatusAppove("N");
                documentMainRepository.save(documentMain);
                documentResponse.setMsg(labelUpSS);
                documentResponse.setStatusMsg(labelSuccess);
                documentResponse.setCodeTypeMsg(labelComplete);
                documentResponse.setDocId(data.getDocId());
                documentResponse.setDataId(data.getId());
            } else {
                documentResponse.setMsg("Nodata In tbl_prosonal");
                documentResponse.setStatusMsg(labelError);
                documentResponse.setCodeTypeMsg(labelError);
            }
        } else {
            documentResponse.setMsg(labelReqId);
            documentResponse.setStatusMsg(labelError);
            documentResponse.setCodeTypeMsg(labelError);
        }
        log.info("successService=DocumentMainService.updateSignApproveAll");

        return documentResponse;
    }
}
