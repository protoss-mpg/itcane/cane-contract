package com.mpg.documentservice.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.mpg.documentservice.export.ExportConstant;
import com.mpg.documentservice.export.ExportFactory;
import com.mpg.documentservice.export.builder.ExportBuilder;
import com.mpg.documentservice.export.builder.ProfileExportBuilder;
import com.mpg.documentservice.export.builder.QuotaYearDetailExportBuilder;
import com.mpg.documentservice.model.ResponseModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExportReportService{
	
	@Value("#{${export.report.profileExport}}")
	private Map<String, String> profileExportConfigMap;
	

	@Value("#{${export.report.quotaYearDetailExport}}")
	private Map<String, String> quotaYearDetailExportConfigMap;

	 
	 @Autowired
	 NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	
	public ResponseEntity genReportByReportCode(String reportCode,Map<String,String> mapParam) {
		ResponseModel response = new ResponseModel();
		try {
			
			
			/* Create Builder */
			ExportBuilder builder = ExportFactory.builder(reportCode);
			
			switch (reportCode){
	            case ExportConstant.REPORT_PROFILE_EXPORT :
	            	builder.setTemplateFileName(profileExportConfigMap.get("templateFileName"));
	    			builder.setFileName(profileExportConfigMap.get("fileName"));
	    			builder.setReportName(profileExportConfigMap.get("reportName"));
	                break;
	            default:
	            	builder.setTemplateFileName(quotaYearDetailExportConfigMap.get("templateFileName"));
	    			builder.setFileName(quotaYearDetailExportConfigMap.get("fileName"));
	    			builder.setReportName(quotaYearDetailExportConfigMap.get("reportName"));
	                break;
	        }

			List  multiDataList = new ArrayList();
			List<Map<String,Object>> dataList = getDataList(mapParam,builder.getSqlStatement());
			
			
			if(dataList.isEmpty()) {
				return ResponseEntity.noContent().build();
			}else {
				multiDataList.add(dataList);
			}
			builder.setDataListBuilder(multiDataList);
			
			if(builder.isGenerateSuccess()) {
				return getResponseEntity(builder.getOutputStream(),builder.getFileName(),builder.getContentType());
			}
		} catch (Exception e) {
			log.error("{}", e);
			response.setSuccess(false);
			response.setMessage(e.getMessage());
		}

		return ResponseEntity.ok(response);
		
	}
	
	/* Find SQL Param */
	private Map<String,Boolean> findSqlParam(String sql){
		Map<String,Boolean> sqlParam = new HashMap();
    	Pattern findParametersPattern = Pattern.compile("(?<!')(:[\\w]*)(?!')");
    	Matcher matcher = findParametersPattern.matcher(sql);
    	while (matcher.find()) { 
    		sqlParam.put(String.valueOf((matcher.group().substring(1))), true);
    	}
    	
    	return sqlParam;
	}
	
	/* Get parameter from map */
	private MapSqlParameterSource getMapSqlParameterSource(Map<String,String> mapParam,Map<String,Boolean> sqlParam) {
		MapSqlParameterSource msps = new MapSqlParameterSource();
		for(Entry<String, String> entry:mapParam.entrySet()){
			String key   = entry.getKey();
			String value = mapParam.get(key);
			
			Boolean isMatchParam = sqlParam.get(key);
			if(isMatchParam.equals(true)) {
				msps.addValue(key,value);
			}
			
		}
		
		return msps;
	}
	
	/* Query Data */
	private List<Map<String,Object>> getDataList(Map<String,String> mapParam,String sqlStatement){
		
		Map<String,Boolean> sqlParam = findSqlParam(sqlStatement);
		
		MapSqlParameterSource msps = this.getMapSqlParameterSource(mapParam, sqlParam);
		
		
		return namedParameterJdbcTemplate.queryForList(sqlStatement, msps);
	}


	/* Set Response Entity */
	private ResponseEntity<Resource> getResponseEntity(ByteArrayOutputStream outputStream,String fileName,String contentType) {
		
		/* Write to outPutStream */
    	ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        HttpHeaders headers = new HttpHeaders();
			headers.setContentType(new MediaType("application", contentType));
	        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+fileName+"\"");
	        headers.setContentLength(outputStream.size());
        return ResponseEntity
	              .ok()
	              .headers(headers)
	              .body(new InputStreamResource(inputStream));
	}
	 
	 
	 
	 

}
